// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Puzzle_Piece.generated.h"

UCLASS()
class WUKONG_API APuzzle_Piece : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzle_Piece();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UPuzzlePieceAttachPointComponent *PuzzlePieceAttachPointComponent;
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent *PuzzlePiece; 
};
