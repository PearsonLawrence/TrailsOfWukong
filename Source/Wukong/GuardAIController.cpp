// All Rights Reserved by Punk Mage Games

#include "GuardAIController.h"

#include "GuardAICharacter.h"
#include "AIBaseCharacter.h"
#include "Engine/World.h"
#include "AI/Navigation/NavigationSystem.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "AITargetPoint.h"
#include "Navigation/CrowdFollowingComponent.h"
AGuardAIController::AGuardAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{

	CurrentState = EFishAIStates::IdleState;
	AcceptanceRadius = 50;
	RoamRadius = 300;
	WaitTime = 0;
	IsGuard = true;
}

void AGuardAIController::BeginPlay()
{
	Super::BeginPlay();
	TargetActor = GetWorld()->GetFirstPlayerController()->GetPawn();
	ThisPawn = GetPawn();
	thisCharacter = Cast<AGuardAICharacter>(GetCharacter());
	SetNewRoamPoint(MoveLocation, RoamRadius);
	IsGuard = thisCharacter->IsGuard;
	if (IsGuard)
	{
		GetGuardTargetPoints();
	}
}

void AGuardAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossesedAI = Cast<AGuardAICharacter>(InPawn);
		if (!ensure(PossesedAI)) { return; }
	}
}

void AGuardAIController::GetGuardTargetPoints()
{
	TArray<AActor*> Temp;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), thisCharacter->PointBP, Temp);
	for (int i = 0; i < Temp.Num(); i++)
	{
		AAITargetPoint* tempPoint = Cast<AAITargetPoint>(Temp[i]);
		if (tempPoint->GuardTargetPoint)
		{
			thisCharacter->TargetPoints.Add(tempPoint);
		}
	}
}

void AGuardAIController::OnPossesedAIDeath()
{
	if (!GetPawn()) { return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}

void AGuardAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!ensure(TargetActor && ThisPawn)) { return; }

	switch (CurrentState)
	{
	case EFishAIStates::IdleState:
		doIdle(DeltaTime);
		break;
	case EFishAIStates::TrackState:
		doTrack();
		break;
	case EFishAIStates::CombatState:
		doCombat();
		break;
	case EFishAIStates::AttackState:
		doAttack();
		break;
	case EFishAIStates::CowerState:
		doCower();
		break;
	}
}

void AGuardAIController::doIdle(float DT)
{
	WaitTime -= DT;

	if (WaitTime <= 0)
	{
		MoveToLocation(MoveLocation, 200, true, true, false, false);
		CurrentDecisionState = EDecisionState::RoamState;
	}

	switch (CurrentDecisionState)
	{
	case EDecisionState::HideState:
		//doCowerDecision();
		break;
	case EDecisionState::StoreState:
		if (thisCharacter->CurrentPoint != nullptr)
			thisCharacter->SetActorRotation(FMath::RInterpTo(thisCharacter->GetActorRotation(), thisCharacter->CurrentPoint->GetActorRotation(), GetWorld()->DeltaTimeSeconds, 5));
		break;
	case EDecisionState::ChaseState:
		//doChaseDecision();
		break;
	}
}

void AGuardAIController::doTrack()
{

}

void AGuardAIController::doCombat()
{

}

void AGuardAIController::doAttack()
{

}

void AGuardAIController::doCower()
{

}

void AGuardAIController::MakeDecision()
{
	switch (CurrentDecisionState)
	{
	case EDecisionState::RoamState:
		doIdleDecision();
		break;
	case EDecisionState::ChatState:
		doChatDecision();
		break;
	case EDecisionState::ChaseState:
		doChaseDecision();
		break;
	}
}

void AGuardAIController::SetNewRoamPoint(FVector &NewPoint, float radius)
{
	NewPoint = UNavigationSystem::GetRandomPointInNavigableRadius(GetWorld(), thisCharacter->GetActorLocation(), radius);
}

void AGuardAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	if (Result.Code == EPathFollowingResult::Success)
	{
		MakeDecision();
	}
	else if (Result.Code == EPathFollowingResult::Blocked)
	{

	}
}


void AGuardAIController::doIdleDecision()
{
	float Rand = FMath::RandRange(0.0f, 100.0f);

	float range = 90.0f;

	if (Rand >= range)
	{
		SetNewRoamPoint(MoveLocation, RoamRadius);
		thisCharacter->CurrentPoint = nullptr;
		WaitTime = FMath::RandRange(0.0f, 15.0f);
	}
	else
	{
		int32 RandomPoint = FMath::RandRange(0, thisCharacter->TargetPoints.Num() - 1);
		if (thisCharacter->TargetPoints[RandomPoint] != nullptr)
		{
			WaitTime = (thisCharacter->CurrentPoint != nullptr) ? thisCharacter->CurrentPoint->PointWaitTime : WaitTime = FMath::RandRange(0.0f, 15.0f);

			MoveLocation = thisCharacter->TargetPoints[RandomPoint]->GetActorLocation();

			thisCharacter->CurrentPoint = thisCharacter->TargetPoints[RandomPoint];
			CurrentDecisionState = thisCharacter->CurrentPoint->StoredState;
		}
		else
		{
			SetNewRoamPoint(MoveLocation, RoamRadius);
			WaitTime = FMath::RandRange(0.0f, 15.0f);
			thisCharacter->CurrentPoint = nullptr;
		}
	}
}

void AGuardAIController::doChaseDecision()
{

}

void AGuardAIController::doStoreDecision()
{

}

void AGuardAIController::doChatDecision()
{

}

void AGuardAIController::doCowerDecision()
{

}


