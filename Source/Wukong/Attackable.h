// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Attackable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UAttackable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WUKONG_API IAttackable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void LightAttack(int AttackNum);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void doLightAttack(int AttackNum);
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void doHeavyAttack(int AttackNum);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Roll(bool doRoll);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void LightThree(bool JumpLeft);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void SwitchMode(bool Switch);

};
