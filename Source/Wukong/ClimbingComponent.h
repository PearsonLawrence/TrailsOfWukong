// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CanGrab.h"
#include "ClimbingComponent.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EClimbStates : uint8
{
	LedgeJumpState 	UMETA(DisplayName = "LedgeJumpState"),
	ClimbUpState 	UMETA(DisplayName = "ClimbUpState"),
	IdleTraceState 	UMETA(DisplayName = "IdleTraceState"),
	CornerTurnState 	UMETA(DisplayName = "CornerTurnState"),
	JumpUpState 	UMETA(DisplayName = "JumpUpState"),
	HangingLedgeState 	UMETA(DisplayName = "HangingLedgeState")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WUKONG_API UClimbingComponent : public UActorComponent, public ICanGrab
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UClimbingComponent();

	
	
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* LeftArrow;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* RightArrow;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* LeftEdge;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* RightEdge;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* UpArrow;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* ToHigh;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* CornerTraceLeft;
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* CornerTraceRight;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	


	// BOOL PROPERTIES
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bIsHanging;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bIsClimbingLedge;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanMoveLeft;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanMoveRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Climbing)
		bool bIsJumping; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Climbing)
		bool bIsJumpingBack;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanJumpRight;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanJumpLeft;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanTurnLeft;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanTurnRight;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bTurningRight;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bTurningLeft;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCanJumpUp;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bTurnedBack;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bHardLanding;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bJumpUpBlocked;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCornerTurnRight;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bCornerTurnLeft;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bHipHang;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bMovingRight;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Climbing)
		bool bMovingLeft;
	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Climbing)
		EClimbStates CurrentState;
	//Float Properties//
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = Acceptance)
		float Acceptance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Acceptance)
		float JumpDelay;
	/////////////////////

	//Physics assets//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Acceptance)
	class UPhysicsAsset* LooseHips;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Acceptance)
	class UPhysicsAsset* TightHips;
	////////////////////

	//VectorProperties//
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = WallInfo)
		FVector WallLocation;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = WallInfo)
		FVector WallNormal;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = WallInfo)
		FVector HeightLocation;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = WallInfo)
		FVector CurrentRot;
	//////////////////////

	//Actors//
	UPROPERTY(EditAnywhere, Category = AttachActors)
		class AActor* FirstActor;
	UPROPERTY(EditAnywhere, Category = AttachActors)
		class ASunWukong* owner;

	UPROPERTY(EditAnywhere, Category = AttachActors)
		class UWukongAnimationInstance* Anim;

	////////////////////
	UFUNCTION(BlueprintCallable)
		void doHipTrace();

	UFUNCTION(BlueprintCallable)
		void doJumpUpTracer();

	UFUNCTION(BlueprintCallable)
		void doForwardTracer();

	UFUNCTION(BlueprintCallable)
		void doHeightTracer();

	UFUNCTION(BlueprintCallable)
		void GrabLedge();
	// Ledge Events
	UFUNCTION(BlueprintCallable)
		void doLeftTracer();
	UFUNCTION(BlueprintCallable)
		void doRightTracer();
	UFUNCTION(BlueprintCallable)
		void MoveLedge();
	UFUNCTION(BlueprintCallable)
		void TurnBackLedge();
	UFUNCTION(BlueprintCallable)
		void TurnForwardLedge();
	UFUNCTION(BlueprintCallable)
		void JumpBackLedge();
	UFUNCTION(BlueprintCallable)
		void JumpEdgeTraceLeft();
	UFUNCTION(BlueprintCallable)
		void TurningLeftTracer();
	UFUNCTION(BlueprintCallable)
		void JumpEdgeTraceRight();
	UFUNCTION(BlueprintCallable)
		void TurningRightTracer();

	UFUNCTION(BlueprintCallable)
		void EndLedge();
	UFUNCTION(BlueprintCallable)
		void JumpRightLedge();
	UFUNCTION(BlueprintCallable)
		void JumpLeftLedge();
	UFUNCTION(BlueprintCallable)
		void JumpUpLedge();
	UFUNCTION(BlueprintCallable)
		void ClimbUpLedge();


	UFUNCTION(BlueprintCallable)
		void MoveOnLedge();

	UFUNCTION(BlueprintCallable)
		bool HipToLedge(float Accept);
	UFUNCTION(BlueprintCallable)
		FVector MoveToLocation();
	UFUNCTION(BlueprintCallable)
		void ResetArrows();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	private:
		void doIdle();

		void doClimbUp();

		void doLedgeJump();

		void doCornerTurn();

		void doJumpUp();

		void doHangingLedge();
};
