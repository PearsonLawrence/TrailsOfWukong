// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BossCombat.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBossCombat : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WUKONG_API IBossCombat
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void doLightAttack(int32 AttackNum);
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void doHeavyAttack(int32 AttackNum);


	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Spin(bool doSpin);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Leap(bool doLeap);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Avoid(int32 doAvoid);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Charge(bool doCharge);

};
