// All Rights Reserved by Punk Mage Games

#include "QuestComponent.h"
#include "Quest.h"

// Sets default values for this component's properties
UQuestComponent::UQuestComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UQuestComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UQuestComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UQuestComponent::FinishQuest(AQuest* Quest)
{
	if (Quest == nullptr) return;

	if (!Quest->ReadyToStart)
	{
		UE_LOG(LogTemp, Warning, TEXT("QUEST NOT READY TO START"));
		return;
	}
	
	int32 Temp = QuestList.Find(Quest);
	if (Temp < 0) return;
	AQuest* TempQuest = QuestList[Temp];

	if (TempQuest == nullptr) return;

	QuestList.RemoveAt(Temp, 1, true);

	int32 CompletedAllQuest = 0;

	for (int i = 0; i < TempQuest->FriendQuest.Num(); i++)
	{
		if (TempQuest->FriendQuest[i] == nullptr) break;

		CompletedAllQuest += (TempQuest->FriendQuest[i]->Completed) ? 1 : 0;
	}

	if (CompletedAllQuest == TempQuest->FriendQuest.Num())
	{
		AddQuest(TempQuest->StoredQuest);
	}


	CompletedQuestList.Push(TempQuest);
}

void UQuestComponent::AddQuest(AQuest* Quest)
{
	int32 Temp = QuestList.Find(Quest);
	if (Temp >= 0 || Quest == nullptr) return;
	
	Quest->ReadyToStart = true;
	QuestList.Push(Quest);
	
}