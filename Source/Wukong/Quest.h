// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Quest.generated.h"

UCLASS()
class WUKONG_API AQuest : public ACharacter
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuest();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Info;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString TrackedName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Tracked;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Completed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool ReadyToStart;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CompletedFriendQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AQuest* StoredQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AQuest*> FriendQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UQuestComponent* StoredPlayerQuest;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
