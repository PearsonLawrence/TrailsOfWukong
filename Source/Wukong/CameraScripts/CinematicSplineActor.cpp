// All Rights Reserved by Punk Mage Games

#include "CinematicSplineActor.h"
#include "Components/SplineComponent.h"

// Sets default values
ACinematicSplineActor::ACinematicSplineActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Base = CreateDefaultSubobject<USceneComponent>((TEXT("Base")));
	Base->AttachTo(RootComponent);
	RootComponent = Base;

	Spline = CreateDefaultSubobject<USplineComponent>((TEXT("Spline")));
	Spline->AttachTo(Base);

	HoldPoint = CreateDefaultSubobject<USceneComponent>((TEXT("HoldPoint")));
	HoldPoint->AttachTo(Base);
}

// Called when the game starts or when spawned
void ACinematicSplineActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACinematicSplineActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

