// All Rights Reserved by Punk Mage Games

#include "GuardAICharacter.h"
#include "GuardAIController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "BaseFishAIController.h"
#include "SunWukong.h"
// Sets default values
AGuardAICharacter::AGuardAICharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetCharacterMovement()->MaxWalkSpeed = 100.0f;
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(FName("PawnSensing"));
	PawnSensor->SensingInterval = .25f; // 4 times per second
	PawnSensor->bOnlySensePlayers = true;
	PawnSensor->SetPeripheralVisionAngle(85.f);
	TurnRate = 10.0f;
	//bUseControllerRotationYaw = false;
	//GetCharacterMovement()->bOrientRotationToMovement = true;

}
void AGuardAICharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	PawnSensor->OnSeePawn.AddDynamic(this, &AGuardAICharacter::OnSeePawn);
	PawnSensor->OnHearNoise.AddDynamic(this, &AGuardAICharacter::OnHearNoise);
}
// Called when the game starts or when spawned
void AGuardAICharacter::BeginPlay()
{
	Super::BeginPlay();
	myGuardAIController = Cast<AGuardAIController>(GetController());
}
// Called every frame
void AGuardAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GetVelocity().Size() > 1)
	{
		FRotator Dir3 = FRotator(0, GetVelocity().Rotation().Yaw, 0);

		SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir3, GetWorld()->DeltaTimeSeconds, TurnRate));
		//myGuardAIController->SetControlRotation();
	}
}
// Called to bind functionality to input
void AGuardAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
void AGuardAICharacter::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{
	const FString VolumeDesc = FString::Printf(TEXT(" at volume %f"), Volume);
	FString message = TEXT("Heard Actor ") + OtherActor->GetName() + VolumeDesc;
	UE_LOG(LogTemp, Warning, TEXT("%s"), *message);
}///

void AGuardAICharacter::OnSeePawn(APawn *OtherPawn)
{

	if (Cast<ASunWukong>(OtherPawn))
	{
		//FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

		SensedPawn = OtherPawn;
	}

	//FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

	// TODO: game-specific logic
}
