# The Trials Of Wukong
The Trials of wukong is a vertical slice of a game, Made for the AIE year 2 major production/final with a team of artists and programmers. The vertical slice consists of hack & slash as well as puzzle elements in order to fight your way through a dream world in order to defeat a boss.


### Game was developed in unreal engine with a mix of c++ and blueprints

## Key Gameplay elements

* Hack n Slash fighting
* Boss fight
* RPG Like Quest System
* Climbing Puzzle



## Where you can find it elsewhere
[Itch.io Page](http://atoxiam.itch.io/wukong)



# Contributers

## Programmers

* Pearson Lawrence
* Xavier A. Melton

## Artists

* Lukas Bartee
* Patrick Alderson
* Trevor White
* Zachary Noah
* Jesse Staples
* Ernest Ziegenfelder

## Audio

* Andrew Pang