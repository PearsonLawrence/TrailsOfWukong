// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Puzzle_Object.generated.h"

UCLASS()
class WUKONG_API APuzzle_Object : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APuzzle_Object();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent *StaticMeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent6;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHoldPointComponent *HoldPointComponent7;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 PuzzlePiecesInPlaceCount;
};