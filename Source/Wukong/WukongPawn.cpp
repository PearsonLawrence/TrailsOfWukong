// All Rights Reserved by Punk Mage Games

#include "WukongPawn.h"


// Sets default values
AWukongPawn::AWukongPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWukongPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWukongPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AWukongPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

}

