// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "CanGrab.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCanGrab : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WUKONG_API ICanGrab
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void CanGrab(bool CanGrabWall, FVector Velocity);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void ClimbLedge(bool isClimbing);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void MoveLeftRight(float Direction);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void JumpLeft(bool JumpLeft);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void JumpRight(bool JumpRight);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void JumpUp(bool JumpUp);


	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void DoubleJump(bool DoubleJump);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void TurnBack(bool TurnBack);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void TurnCornerLeft(bool TurnL);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void TurnCornerRight(bool TurnR);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void EnableWeapon(bool grow); // called to BP to disable weapon

};
