// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "SideScrollerCamera.generated.h"


UCLASS(config = Game)
class WUKONG_API ASideScrollerCamera : public ACameraActor
{
	GENERATED_BODY()

	ASideScrollerCamera();
		// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	class ASunWukong* followActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FTransform holdPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	AActor* Boss;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	class ACinematicSplineActor* CinematicLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float Xoffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float Yoffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float Zoffset;

protected:

	void FollowMode(float DeltaTime);

	void LockedMode(float DeltaTime);

	void LockedandLookat(float DeltaTime);

	void LockedZPose(float DeltaTime);

	void LookandFollowAtAngle(float DeltaTime);

	void SpiralFollow(float DeltaTime);

	void PlayerControl(float DeltaTime);

	void BossCamera(float DeltaTime);

	void CinematicCamera(float DeltaTime);
};
