// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Damageable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDamageable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WUKONG_API IDamageable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void TakeWeaponDamage(float Damage, bool fromPlayer, FVector LaunchDir, FVector ImpactLocation);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void Stun(float Time);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void StunLeft(float Time);
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void StunRight(float Time);

	
};
