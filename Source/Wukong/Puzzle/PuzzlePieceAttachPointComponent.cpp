// All Rights Reserved by Punk Mage Games

#include "PuzzlePieceAttachPointComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"


// Sets default values for this component's properties
UPuzzlePieceAttachPointComponent::UPuzzlePieceAttachPointComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Creating the collision box
	AttachPointCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Attach Point Box Component"));
	AttachPointCollisionBox->InitBoxExtent(FVector(10, 10, 10));
	AttachPointCollisionBox->SetCollisionProfileName("Trigger");

}


// Called when the game starts
void UPuzzlePieceAttachPointComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPuzzlePieceAttachPointComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

