// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GuardAICharacter.generated.h"

UCLASS()
class WUKONG_API AGuardAICharacter : public ACharacter
{
	GENERATED_BODY()

private:
	class AGuardAIController* myGuardAIController;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;
public:
	AGuardAICharacter();


	
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
		class UPawnSensingComponent* PawnSensor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targets)
		class APawn* SensedPawn;

	UFUNCTION()
		void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
		void OnSeePawn(APawn *OtherPawn);

	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool IsGuard;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool IsLeader;
	UPROPERTY(EditAnywhere, Category = "target")
		TArray<class AGuardAICharacter*> Squad;

	UPROPERTY(EditAnywhere, Category = "Turning") // consider EditDefaultsOnly
		float TurnRate;
	UPROPERTY(EditAnywhere, Category = "target")
		TArray<class AAITargetPoint*> TargetPoints;

	UPROPERTY(EditAnywhere, Category = "target")
		TSubclassOf<class AAITargetPoint> PointBP;

	UPROPERTY(EditAnywhere, Category = "target")
		class AAITargetPoint* CurrentPoint;

};
