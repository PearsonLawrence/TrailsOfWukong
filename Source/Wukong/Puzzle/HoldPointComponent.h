// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "HoldPointComponent.generated.h"

UCLASS()
class WUKONG_API UHoldPointComponent : public UStaticMeshComponent
{

	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHoldPointComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	

public:
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent *HoldPointCollisionBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 HoldPointValue;
};
