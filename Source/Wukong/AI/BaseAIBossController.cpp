// All Rights Reserved by Punk Mage Games

#include "BaseAIBossController.h"




#include "AIBaseBoss.h"
#include "Engine/World.h"
#include "AI/Navigation/NavigationSystem.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AITargetPoint.h"
#include "Navigation/CrowdFollowingComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BossCombat.h"

ABaseAIBossController::ABaseAIBossController(const FObjectInitializer& ObjectInitializer)
{

	CurrentState = EGuardianStates::IdleState;
	AcceptanceRadius = 200;
	RoamRadius = 500;
	AttackCooldown = 2;
}

void ABaseAIBossController::BeginPlay()
{
	Super::BeginPlay();
	TargetActor = GetWorld()->GetFirstPlayerController()->GetPawn();
	ThisPawn = GetPawn();
	thisCharacter = Cast<AAIBaseBoss>(GetCharacter());
	
}

void ABaseAIBossController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossesedAI = Cast<AAIBaseBoss>(InPawn);
		if (!ensure(PossesedAI)) { return; }
	}
}


void ABaseAIBossController::OnPossesedAIDeath()
{
	if (!GetPawn()) { return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}

void ABaseAIBossController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (thisCharacter->bIsActive)
	{
		AttackCooldown -= DeltaTime;
		MoveSwitchTime -= DeltaTime;
		DodgeCooldown -= DeltaTime;
		if (!ensure(TargetActor && ThisPawn)) { return; }

		if (thisCharacter->bIsActive)
		{

			switch (CurrentState)
			{

			case EGuardianStates::IdleState:
				doIdle(DeltaTime);
				break;
			case EGuardianStates::SeekState:
				doTrack();
				break;
			case EGuardianStates::AvoidState:
				doCombat(DeltaTime);
				break;
			case EGuardianStates::ActionState:
				doAction();
				break;
			case EGuardianStates::DodgeState:
				doDodge();
				break;
			case EGuardianStates::AttackState:
				doAttack();
				break;
			}
			//HandleMovement();
		
		}

		if (AttackCooldown <= 0 && !Attacking)
		{
			CurrentState = EGuardianStates::ActionState;
		}
	}
}

void ABaseAIBossController::HandleMovement()
{
	//switch (CurrentAttackSate)
	//{
	//case EBossMoveStates::IdleState:
	//	MoveTowards(MoveLocation, thisCharacter->RoamWalkSpeed);
	//	LookTowards(MoveLocation, 2);
	//	break;
	//case EBossMoveStates::SpinAttackState:
	//	MoveTowards(MoveLocation, thisCharacter->RoamWalkSpeed);
	//	LookTowards(MoveLocation, 2);
	//	break;
	//case EBossMoveStates::AvoidBackState:
	//	//MoveTowards(MoveLocation, thisCharacter->RoamWalkSpeed);
	//	//LookTowards(MoveLocation, 2);
	//	break;
	//case EBossMoveStates::AvoidLeftState:
	//	break;
	//case EBossMoveStates::AvoidRightState:
	//	break;
	//case EBossMoveStates::AttackLightState:
	//	MoveTowards(CachedLocation, thisCharacter->RoamWalkSpeed);
	//	LookTowards(CachedLocation, 2);
	//	break;
	//case EBossMoveStates::LeapState:
	//	MoveTowards(CachedLocation, thisCharacter->RoamWalkSpeed);
	//	LookTowards(CachedLocation, 2);
	//	break;
	//case EBossMoveStates::WalkState:
	//	MoveTowards(MoveLocation, thisCharacter->RoamWalkSpeed);
	//	LookTowards(MoveLocation, 2);
	//	break;
	//default:
	//	break;
	//}
}

void ABaseAIBossController::doIdle(float DT)
{
	if (thisCharacter->BossTarget != nullptr)
	{
		//CurrentAttackSate = EBossMoveStates::IdleState;
		
		thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->RoamWalkSpeed;
		LookTowards(thisCharacter->BossTarget->GetActorLocation(), 2);

		float Dist = (thisCharacter->GetActorLocation() - thisCharacter->BossTarget->GetActorLocation()).Size();
		
		doStrafe();

		if (DodgeCooldown <= 0)
		{
			CurrentState = EGuardianStates::DodgeState;
		}

		if (Dist <= 200)
		{
			CurrentState = EGuardianStates::ActionState;
		}
	}
}

void ABaseAIBossController::doTrack()
{

}

void ABaseAIBossController::doCombat(float DT)
{


}

void ABaseAIBossController::doAction()
{
	if (thisCharacter->BossTarget == nullptr) { return; }

	MoveLocation = thisCharacter->BossTarget->GetActorLocation();

	float Dist = (thisCharacter->GetActorLocation() - thisCharacter->BossTarget->GetActorLocation()).Size();
	if (Dist >= 900)
	{
		thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->LeapWalkSpeed;
		MoveToLocation((Attacking) ? CachedLocation : MoveLocation, AcceptanceRadius, false, true, false, false);
		//MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
		if (!Attacking)
		{
			CachedLocation = MoveLocation;
			distanceDecision();
		}
	}
	else if (Dist >= 700)
	{
		MoveToLocation((Attacking) ? CachedLocation : MoveLocation, AcceptanceRadius, false, true, false, false);
		if (!Attacking)
		{
			CachedLocation = MoveLocation;
			thisCharacter->Execute_Spin(thisCharacter, true);

		}
	}
	else
	{
		LookTowards(MoveLocation, 1);

		MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
		if (!Attacking)
		{
			CachedLocation = MoveLocation;
			closeDecision();
		}
		else
		{
			//MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
		}
	}
}


void ABaseAIBossController::doAttack()
{
	if (thisCharacter->BossTarget == nullptr) { return; }

	MoveLocation = thisCharacter->BossTarget->GetActorLocation();

	float Dist = (thisCharacter->GetActorLocation() - thisCharacter->BossTarget->GetActorLocation()).Size();
	if (Dist >= 600)
	{

		//MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
		if (!Attacking)
		{
			distanceDecision();
		}
	}
	else if (Dist >= 500)
	{
		if (!Attacking)
		{
			MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
			thisCharacter->Execute_Spin(thisCharacter, true);

		}
	}
	else
	{
		if (!Attacking)
		{
			closeDecision();
		}
		else
		{
			//MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
		}
	}
}

void ABaseAIBossController::MoveTowards(FVector ToLocation, float MoveSpeed)
{
	FVector Dir = (thisCharacter->GetActorLocation() - ToLocation).GetSafeNormal() * -1;
	thisCharacter->AddMovementInput(Dir, MoveSpeed);
}
void ABaseAIBossController::LookTowards(FVector ToLocation, float TurnSpeed)
{
	FVector Dir = (thisCharacter->GetActorLocation() - ToLocation).GetSafeNormal() * -1;;
	FRotator Rot = Dir.Rotation();
	Rot.Pitch = 0;
	Rot.Roll = 0;
	thisCharacter->SetActorRotation(FMath::RInterpTo(thisCharacter->GetActorRotation(), Rot, GetWorld()->DeltaTimeSeconds, TurnSpeed));
}
void ABaseAIBossController::DoChase(AActor* target)
{
	

}


void ABaseAIBossController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	if (Result.Code == EPathFollowingResult::Success)
	{
		//CurrentState = EGuardianStates::ActionState;
	}
	else if (Result.Code == EPathFollowingResult::Blocked)
	{

	}
}



EGuardianStates ABaseAIBossController::GetCurrentState()
{
	return CurrentState;
}
void ABaseAIBossController::SetCurrentState(EGuardianStates State)
{
	CurrentState = State;
}

void ABaseAIBossController::distanceDecision()
{
	int RandInt = FMath::RandRange(0, 5);

	if (RandInt == 0 || RandInt == 2 || RandInt == 4)
	{

		thisCharacter->Execute_Leap(thisCharacter, true);
	}
	else
	{

		thisCharacter->Execute_Charge(thisCharacter, true);
	}
}

void ABaseAIBossController::closeDecision()
{
	float Randfloat = FMath::RandRange(0.0f, 100.0f);

	int AttackNum = FMath::RandRange(0, 2);

	if (Randfloat <= 70)
	{
		//MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
		thisCharacter->Execute_doLightAttack(thisCharacter, AttackNum);

		
			thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->AttackWalkSpeed;
		
	}
	else
	{
		DodgeCooldown = 0;
		CurrentState = EGuardianStates::DodgeState;
		Dodging = false;
		Attacking = false;
	}
}

void ABaseAIBossController::doAvoid()
{
	int32 RandInt = FMath::RandRange(0, 3);

	switch (RandInt)
	{
	case 0:
		thisCharacter->Execute_Spin(thisCharacter, true);
		break;
	case 1:
		thisCharacter->Execute_Avoid(thisCharacter, 1);
		break;
	case 2:
		thisCharacter->Execute_Avoid(thisCharacter, 2);
		break;
	case 3:
		thisCharacter->Execute_Avoid(thisCharacter, 3);
		break;
	}
}

void ABaseAIBossController::doStrafe()
{
	if (MoveSwitchTime <= 0)
	{
		int32 randInt = FMath::RandRange(0, 2);
		switch (randInt)
		{
		case 0:
			CurrentAttackSate = EBossAttackStates::StrafeLeft;
			break;
		case 1:
			CurrentAttackSate = EBossAttackStates::StrafeRight;
			break;
		case 2:
			CurrentAttackSate = EBossAttackStates::WalkBack;
			break;
		}
		MoveSwitchTime = FMath::RandRange(1.5f,5.5f);
	}

	FVector Dir = (thisCharacter->GetActorRightVector() * -300);
	FVector Dir1 = (thisCharacter->GetActorRightVector() * 300);
	switch (CurrentAttackSate)
	{
	case EBossAttackStates::StrafeLeft:
		MoveLocation = Dir + thisCharacter->GetActorLocation();
		MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
		break;
	case EBossAttackStates::StrafeRight:
		MoveLocation = Dir1 + thisCharacter->GetActorLocation();
		MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
		break;
	case EBossAttackStates::WalkBack:
		MoveLocation = thisCharacter->BossTarget->GetActorLocation();
		MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
		break;
	}
}

void ABaseAIBossController::doDodge()
{
	thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->DodgeWalkSpeed;
	LookTowards(thisCharacter->BossTarget->GetActorLocation(), 2);

	if (!Dodging && !Attacking)
	{
		int32 randInt = FMath::RandRange(0, 2);
		switch (randInt)
		{
		case 0:
			CurrentAttackSate = EBossAttackStates::AvoidBackState;
			thisCharacter->Execute_Avoid(thisCharacter, 1);
			break;
		case 1:
			CurrentAttackSate = EBossAttackStates::AvoidLeftState;
			thisCharacter->Execute_Avoid(thisCharacter, 2);
			break;
		case 2:
			CurrentAttackSate = EBossAttackStates::AvoidRightState;
			thisCharacter->Execute_Avoid(thisCharacter, 3);
			break;
		}
		Dodging = true;
	}

	if (DodgeCooldown <= 0)
	{
		FVector Dir = (thisCharacter->GetActorRightVector()) * 300;
		FVector Dir1 = (thisCharacter->GetActorRightVector()) * -300;
		FVector Dir2 = (thisCharacter->GetActorForwardVector()) * -300;
		switch (CurrentAttackSate)
		{
		case EBossAttackStates::AvoidRightState:
			MoveLocation = Dir + thisCharacter->GetActorLocation();
			MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
			break;
		case EBossAttackStates::AvoidLeftState:
			MoveLocation = Dir1 + thisCharacter->GetActorLocation();
			MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
			break;
		case EBossAttackStates::AvoidBackState:
			MoveLocation = Dir2 + thisCharacter->GetActorLocation();
			MoveToLocation(MoveLocation, AcceptanceRadius, false, true, false, false);
			break;
		}
	}
	else
	{
		Dodging = false;

		float randFloat = FMath::RandRange(0.0f, 100.0f);
		if (randFloat <= 50)
		{
			CurrentState = EGuardianStates::ActionState;
		}
		else
		{
			CurrentState = EGuardianStates::IdleState;
		}
	}
}