// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "QuestComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WUKONG_API UQuestComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UQuestComponent();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ShowQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AQuest* TrackedQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class AQuest*> QuestList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class AQuest*> CompletedQuestList;

	UFUNCTION(BlueprintCallable)
		void FinishQuest(class AQuest* Quest);

	UFUNCTION(BlueprintCallable)
		void AddQuest(class AQuest* Quest);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
