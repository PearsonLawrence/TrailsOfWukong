// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseFishAIController.h"
#include "GuardAIController.generated.h"

/**
 * 
 */
UCLASS()
class WUKONG_API AGuardAIController : public AAIController
{
	GENERATED_BODY()

public:
	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "target")
	EFishAIStates CurrentState;
	UPROPERTY(EditAnywhere, Category = "target")
	EDecisionState CurrentDecisionState;

	AGuardAIController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float AcceptanceRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float RoamRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool IsGuard;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		FVector MoveLocation;
	UPROPERTY(EditAnywhere, Category = "target")
		AActor* TargetActor;
	UPROPERTY(EditAnywhere, Category = "target")
		float WaitTime;


protected:


	class AGuardAICharacter* thisCharacter;
	class APawn* ThisPawn;
private:

	virtual void SetPawn(APawn* InPawn) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result) override;

	UFUNCTION()
		void OnPossesedAIDeath();
	UFUNCTION()
		void SetNewRoamPoint(FVector &NewPoint, float radius);

	////////////// State Actions
	void doIdle(float DT);
	void doTrack();
	void doCombat();
	void doAttack();
	void doCower();

	////////////// State Decisions
	void doIdleDecision();
	void doChaseDecision();
	void doChatDecision();
	void doStoreDecision();
	void doCowerDecision();

	void MakeDecision();

	void GetCivilianTargetPoints();
	void GetGuardTargetPoints();
	
	
	
};
