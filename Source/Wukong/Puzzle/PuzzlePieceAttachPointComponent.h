// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "PuzzlePieceAttachPointComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WUKONG_API UPuzzlePieceAttachPointComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPuzzlePieceAttachPointComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere)
		class UBoxComponent *AttachPointCollisionBox;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 PuzzlePieceValue;

};
