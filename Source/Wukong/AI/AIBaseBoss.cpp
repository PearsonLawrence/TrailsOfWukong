// All Rights Reserved by Punk Mage Games

#include "AI/AIBaseBoss.h"
#include "BaseAIBossController.h"
#include "HealthComponent.h"
#include "CombatComponent.h"

// Sets default values
AAIBaseBoss::AAIBaseBoss()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	HealthComponent->isPlayer = false;

	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("Combat Component"));
}

// Called when the game starts or when spawned
void AAIBaseBoss::BeginPlay()
{
	Super::BeginPlay();
	myAIController = Cast<ABaseAIBossController>(GetController());
	
}

// Called every frame
void AAIBaseBoss::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAIBaseBoss::SetBossActive(bool bActive, AActor* Target)
{
	bIsActive = bActive;
	BossTarget = Target;
	if (myAIController != nullptr)
	{
		myAIController->bIsActive = bActive;
	}
}