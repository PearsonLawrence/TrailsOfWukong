// All Rights Reserved by Punk Mage Games

#include "AITargetPoint.h"


// Sets default values
AAITargetPoint::AAITargetPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAITargetPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAITargetPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

