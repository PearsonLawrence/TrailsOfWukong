// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseFishAIController.h"
#include "AITargetPoint.generated.h"


UCLASS()
class WUKONG_API AAITargetPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAITargetPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool GuardTargetPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CowerPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PointWaitTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EDecisionState StoredState;
};
