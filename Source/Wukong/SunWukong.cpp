// All Rights Reserved by Punk Mage Games

#include "SunWukong.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraActor.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Public/DrawDebugHelpers.h"
#include "ClimbingComponent.h"
#include "CameraScripts/SideScrollerCamera.h"
#include "CombatComponent.h"
#include "CanGrab.h"
#include "WeaponMeshComponent.h"
#include "HealthComponent.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ArrowComponent.h"
#include "AI/AIBaseBoss.h"
#include "AI/AICharacter.h"
#include "QuestComponent.h"
// Sets default values
ASunWukong::ASunWukong()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	MaxJumpCount = 2;
	VelocityTurnSpeed = 10;

	CurrentState = EPlayerStates::IdleState;

	WalkSpeed = 600;
	SprintSpeed = 1000;
	CombatWalkSpeed = 400;
	CombatSprintSpeed = 550;
	AttackSpeed = 30;

	ClimbingComponent = CreateDefaultSubobject<UClimbingComponent>(TEXT("Climbing Component"));
	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("Combat Component"));
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	QuestComponent = CreateDefaultSubobject<UQuestComponent>(TEXT("Quest Component"));
	Staff = CreateDefaultSubobject<UWeaponMeshComponent>(TEXT("Staff"));
	Staff->AttachTo(GetMesh(), FName("Slave_R_middle"));
	//Staff->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName("Slave_R_middle"));
	
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->AddLocalOffset(FVector(0,0,GetCapsuleComponent()->GetScaledCapsuleHalfHeight()));
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = false; // Rotate the arm based on the controller
	CameraBoom->bInheritYaw = false; // Rotate the arm based on the controller
	CameraBoom->bInheritPitch = false; // Rotate the arm based on the controller
	CameraBoom->bInheritRoll = false; // Rotate the arm based on the controller

	CamHoldPoint = CreateDefaultSubobject<UArrowComponent>(FName("CamHoldPoint"));
	CamHoldPoint->SetupAttachment(CameraBoom);
	
	
}

void ASunWukong::SetCamera(ASideScrollerCamera* Cam)
{
	FollowCamera = Cam;
}

void ASunWukong::SetCameraState(ECameraStates State, AActor* inputActor)
{
	CameraState = State;
	if(inputActor != nullptr)
	FollowCamera->Boss = inputActor;
}

void ASunWukong::BeginPlay()
{
	Super::BeginPlay();

}

void ASunWukong::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	//PlayerInputComponent->BindAction("Jump", IE_Released, this, &ASunWukong::JumpBack);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASunWukong::PlayerJump);

	PlayerInputComponent->BindAction("TurnRight", IE_Pressed, this, &ASunWukong::TurnRight);
	PlayerInputComponent->BindAction("TurnLeft", IE_Pressed, this, &ASunWukong::TurnLeft);

	PlayerInputComponent->BindAxis("Turn", this, &ASunWukong::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ASunWukong::LookUpAtRate);

	PlayerInputComponent->BindAction("Back", IE_Pressed, this, &ASunWukong::Back);
	PlayerInputComponent->BindAction("Forward", IE_Released, this, &ASunWukong::Forward);

	PlayerInputComponent->BindAction("LightAttack", IE_Pressed, this, &ASunWukong::doLightAttack);
	PlayerInputComponent->BindAction("HeavyAttack", IE_Pressed, this, &ASunWukong::doHeavyAttack);
	PlayerInputComponent->BindAction("SwitchMode", IE_Pressed, this, &ASunWukong::SwitchMode);
	PlayerInputComponent->BindAction("CombatDodge", IE_Pressed, this, &ASunWukong::doDodge);
	PlayerInputComponent->BindAction("ShowQuest", IE_Pressed, this, &ASunWukong::ShowQuest);

	//PlayerInputComponent->BindAction("Forward", IE_Released, this, &ASunWukong::Forward);

	PlayerInputComponent->BindAxis("Sprint", this, &ASunWukong::doSprint);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASunWukong::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASunWukong::MoveRight);
}

void ASunWukong::ShowQuest()
{
	QuestComponent->ShowQuest = (QuestComponent->ShowQuest) ? false : true;//
}

void ASunWukong::doDodge()
{
	if (!bInLockedCinematic)
	{
		if (CurrentState != EPlayerStates::DodgeState)
		{
			if (GetVelocity().Size() > 1)
			{
				FRotator Dir3 = FRotator(0, GetVelocity().Rotation().Yaw, 0);
				SetActorRotation(Dir3);
			}
			Execute_EnableWeapon(this, true);
			CurrentState = EPlayerStates::DodgeState;
			Execute_Roll(this, true);
		}
	}
}

void ASunWukong::doSprint(float Value)
{
	if (!bInMoveableCinematic)
	{
		if (CurrentState == EPlayerStates::IdleState)
		{
			GetCharacterMovement()->MaxWalkSpeed = (Value > 0) ? SprintSpeed : WalkSpeed;
		}
		else
		{
			GetCharacterMovement()->MaxWalkSpeed = (Value > 0) ? CombatSprintSpeed : CombatWalkSpeed;
		}
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = CinematicWalkSpeed;
	}
}

void ASunWukong::TurnAtRate(float Rate)
{
	if (!bInLockedCinematic)
	{
		FVector Dir = (CameraBoom->GetComponentLocation() - FollowCamera->GetActorLocation()).GetSafeNormal() * -1;
		// calculate delta for this frame from the rate information
		FRotator Rot = FRotator(0, Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds(), 0);

		if (CameraState != ECameraStates::PlayerControlCamera)
		{
			CameraBoom->SetWorldRotation(Dir.Rotation());
		}
		else
		{
			CameraBoom->AddWorldRotation(Rot);
		}

		SideRate = Rate;
	}
}

void ASunWukong::LookUpAtRate(float Rate)
{
	FVector Dir = (CameraBoom->GetComponentLocation() - FollowCamera->GetActorLocation()).GetSafeNormal() * -1;
	
	if (!bInLockedCinematic)
	{
		if (CameraState == ECameraStates::PlayerControlCamera)
		{
			// calculate delta for this frame from the rate information
			FRotator Rot;
			if (Rate > 0)
			{
				Rot = (CameraBoom->GetComponentRotation().Pitch <= 80)
					? FRotator(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds(), 0, 0) : FRotator(0, 0, 0);
			}
			else
			{
				Rot = (CameraBoom->GetComponentRotation().Pitch >= -80)
					? FRotator(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds(), 0, 0) : FRotator(0, 0, 0);
			}
			CameraBoom->AddRelativeRotation(Rot);
			uprate = Rate;
		}
		else
		{
			CameraBoom->SetWorldRotation(Dir.Rotation());
		}
	}
}///


void ASunWukong::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bInLockedCinematic)
	{
		switch (CurrentState)
		{
		case EPlayerStates::IdleState:
			GetCharacterMovement()->MaxWalkSpeed = (!Sprint) ? WalkSpeed : SprintSpeed;
			break;
		case EPlayerStates::CombatState:
			GetCharacterMovement()->MaxWalkSpeed = (!Sprint) ? CombatWalkSpeed : CombatSprintSpeed;
			break;
		case EPlayerStates::AttackState:
			GetCharacterMovement()->MaxWalkSpeed = AttackSpeed;
			break;
		}

		if (FollowCamera != nullptr)
		{
			FVector Rot = FollowCamera->GetActorForwardVector();
			Rot.Z = 0;
			FVector temp = Rot.GetSafeNormal();

			MovementDirection = temp;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Missing Cam"));
		}

		FRotator Dir3 = FRotator(0, GetVelocity().Rotation().Yaw, 0);

		if (!GetCharacterMovement()->IsFalling())
		{
			JumpCount = 0;
		}

		//if (CurrentState != EPlayerStates::DodgeState)
		//{
			if (GetVelocity().Size() > 1 && !ClimbingComponent->bIsHanging && !ClimbingComponent->bTurningLeft && !ClimbingComponent->bTurningRight)
			{
				if (CameraState != ECameraStates::BossCamState || CurrentState == EPlayerStates::IdleState || CurrentState == EPlayerStates::DodgeState)
				{
					Controller->SetControlRotation(FMath::RInterpTo(GetActorRotation(), Dir3, GetWorld()->DeltaTimeSeconds, VelocityTurnSpeed));
				}
				else//
				{
					FVector Dir = (GetActorLocation() - FollowCamera->Boss->GetActorLocation()).GetSafeNormal() * -1;
					//Dir.Pitch = 0;
					//Dir.Roll = 0;
					FRotator Rot = Dir.Rotation();
					Rot.Pitch = 0;
					Rot.Roll = 0;

					SetActorRotation(FMath::RInterpTo(GetActorRotation(), Rot, GetWorld()->DeltaTimeSeconds, 100));
				}
			}
		//}


		CameraBoom->SetWorldRotation(FRotator(FMath::Clamp(CameraBoom->GetComponentRotation().Pitch, -90.0f, 90.0f), CameraBoom->GetComponentRotation().Yaw, 0));
	}
}

void ASunWukong::PlayerJump()
{
		doIdleJump();
}

void ASunWukong::doIdleJump()
{
	if (!bInLockedCinematic)
	{
		if (ClimbingComponent != nullptr)
		{
			if (ClimbingComponent->bTurnedBack && ClimbingComponent->bIsHanging && !ClimbingComponent->bIsJumping)
			{
				ClimbingComponent->EndLedge();
				LaunchCharacter(GetActorForwardVector() * -200 + FVector(0, 0, 600), false, false);

				SetActorRotation(GetActorRotation() - FRotator(0, 180, 0));

				Execute_TurnBack(this, true);

				ClimbingComponent->bTurnedBack = false;
				ClimbingComponent->FirstActor = nullptr;
				ClimbingComponent->JumpDelay = .5f;
				return;
			}

			if (ClimbingComponent->bIsHanging)
			{
				if (!ClimbingComponent->bTurnedBack)
				{
					if (ClimbingComponent->bCanJumpRight && MovementRight > 0 && !ClimbingComponent->bCornerTurnRight)
					{
						ClimbingComponent->JumpRightLedge();
					}
					else if (ClimbingComponent->bCanJumpLeft && MovementRight < 0 && !ClimbingComponent->bCornerTurnLeft)
					{
						ClimbingComponent->JumpLeftLedge();
					}
					else if (ClimbingComponent->bCanJumpUp)
					{
						ClimbingComponent->JumpUpLedge();
					}
					else if (!ClimbingComponent->bJumpUpBlocked)
					{
						ClimbingComponent->ClimbUpLedge();
						if (ClimbingComponent->bHipHang)
						{

							ClimbingComponent->bHipHang = false;
							GetMesh()->SetPhysicsAsset(ClimbingComponent->TightHips);
							GetMesh()->SetSimulatePhysics(false);

						}
					}
				}

			}
			else
			{
				if (JumpCount < MaxJumpCount)
				{
					if (JumpCount == 0)
					{
						Jump();
						CurrentState = EPlayerStates::IdleState;
						Execute_EnableWeapon(this, false);
					}
					else
					{
						GetCharacterMovement()->Launch(GetActorUpVector() * 700 + GetVelocity());
						Execute_DoubleJump(this, true);
					}
					JumpCount++;
				}
			}
		}
	}
}

void ASunWukong::doCombatJump()
{

}

void ASunWukong::MoveForward(float Value)
{
	if (!bInLockedCinematic)
	{
		switch (CurrentState)
		{
		case EPlayerStates::IdleState:
			doIdleMoveForward(Value);
			break;
		case EPlayerStates::CombatState:
			doCombatMoveForward(Value);
			break;
		case EPlayerStates::AttackState:
			doAttackMoveForward(Value);
			break;
		}
	}
}
void ASunWukong::MoveRight(float Value)
{

	switch (CurrentState)
	{
	case EPlayerStates::IdleState:
		doIdleMoveRight(Value);
		break;
	case EPlayerStates::CombatState:
		doCombatMoveRight(Value);
		break;
	case EPlayerStates::AttackState:
		doAttackMoveRight(Value);
		break;
	}
	MovementRight = Value;
	

}

void ASunWukong::doIdleMoveForward(float Value)
{

	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		 //find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		//
		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		AddMovementInput(MovementDirection, Value);

	}
}
void ASunWukong::doCombatMoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		//find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		//
		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		
		AddMovementInput(MovementDirection, Value);

	}
}
void ASunWukong::doAttackMoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		//find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		//
		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		FVector Dir = FollowCamera->GetActorForwardVector() * Value;
		FRotator Rot = Dir.Rotation();

		Rot.Pitch = 0;
		Rot.Roll = 0;

		if (FollowCamera != nullptr)
			SetActorRotation(FMath::RInterpTo(GetActorRotation(), Rot, GetWorld()->DeltaTimeSeconds, 2));
	}
}

void ASunWukong::doIdleMoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		if (FollowCamera != nullptr)
			AddMovementInput(FollowCamera->GetActorRightVector(), Value);
	}
	if (ClimbingComponent->bIsHanging)
	{
		ClimbingComponent->MoveLedge();
	}

}
void ASunWukong::doCombatMoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		if (FollowCamera != nullptr)
			AddMovementInput(FollowCamera->GetActorRightVector(), Value);
	}
}
void ASunWukong::doAttackMoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && !ClimbingComponent->bIsHanging)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		FVector Dir = FollowCamera->GetActorRightVector() * Value;

		// add movement in that direction
		if (FollowCamera != nullptr)
			SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir.Rotation(), GetWorld()->DeltaTimeSeconds, 2));
	}
}
void ASunWukong::TurnRight()
{
	if (!bInLockedCinematic)
	{
		if (ClimbingComponent->bIsHanging && !ClimbingComponent->bIsJumping)
		{
			if (ClimbingComponent->bCanTurnRight && !ClimbingComponent->bTurningRight)
			{
				Execute_TurnCornerRight(this, true);
			}
		}
	}
}
void ASunWukong::TurnLeft()
{
	if (!bInLockedCinematic)
	{
		if (ClimbingComponent->bIsHanging && !ClimbingComponent->bIsJumping)
		{
			if (ClimbingComponent->bCanTurnLeft && !ClimbingComponent->bTurningLeft)
			{
				Execute_TurnCornerLeft(this, true);
			}
		}
	}
}

void ASunWukong::Back()
{
	if (!bInLockedCinematic)
	{
		if (ClimbingComponent != nullptr && GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Flying)
		{
			if (ClimbingComponent->bTurnedBack)
			{
				ClimbingComponent->EndLedge();
				ClimbingComponent->bIsHanging = false;
				GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
			}
			else
			{
				if (ClimbingComponent->bTurnedBack)
				{
					ClimbingComponent->bTurnedBack = false;

					ClimbingComponent->EndLedge();
				}
				else
				{
					ClimbingComponent->bTurnedBack = true;

				}
			}
		}
	}
}
void ASunWukong::Forward()
{
	if (!bInLockedCinematic)
	{
		if (ClimbingComponent != nullptr)
		{
			if (ClimbingComponent->bTurnedBack)
			{
				//auto Anim = Cast<ICanGrab>(GetMesh()->GetAnimInstance());

				ClimbingComponent->bTurnedBack = false;
			}
		}
	}
}
void ASunWukong::doRoll()
{
	if (!bInLockedCinematic)
	{
		if ((Controller != NULL) && !ClimbingComponent->bIsHanging)
		{
			//find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);
			//
			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

			AddMovementInput(GetActorForwardVector(), 1);

		}
	}
}
void ASunWukong::doLightAttack()
{
	if (!bInLockedCinematic && !bInMoveableCinematic)
	{
		if (!CombatComponent->AttackPressed) // Checks to see if attack has already been pressed
		{
			CombatComponent->AttackPressed = true;
			if (CombatComponent->AttackNum == 0)
			{
				Staff->HeavyAttack = false;
				Staff->WeaponDamageAmount = Staff->LightDamage;

				Execute_doLightAttack(this, CombatComponent->AttackNum); //Executes light attack from the start if light one is not active
			}

		}
	}
}
void ASunWukong::doHeavyAttack()
{
	if (!bInLockedCinematic && !bInMoveableCinematic)
	{
		if (!CombatComponent->AttackPressed)
		{
			Staff->HeavyAttack = true;
			Staff->WeaponDamageAmount = Staff->HeavyDamage;

			Execute_doHeavyAttack(this, CombatComponent->AttackNum);
		}
	}
}
EPlayerStates ASunWukong::GetCurrentState()
{
	return CurrentState;
}
void ASunWukong::SetCurrentState(EPlayerStates State)
{
	CurrentState = State;
}
void ASunWukong::SwitchMode()
{
	if (!bInLockedCinematic && !bInMoveableCinematic)
	{
		if (CurrentState != EPlayerStates::AttackState)
		{
			Execute_SwitchMode(this, true);
			CurrentState = (CurrentState == EPlayerStates::IdleState) ? EPlayerStates::CombatState : EPlayerStates::IdleState;

		}
	}
	else
	{
		CurrentState = EPlayerStates::IdleState;
	}
}