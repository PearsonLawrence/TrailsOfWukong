// All Rights Reserved by Punk Mage Games

#include "HoldPointComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"


// Sets default values for this component's properties
UHoldPointComponent::UHoldPointComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	


	//Creating the collision box
	HoldPointCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hold Point Box Component"));
	HoldPointCollisionBox->InitBoxExtent(FVector(10, 10, 10));
	HoldPointCollisionBox->SetCollisionProfileName("Trigger");
	
}


// Called when the game starts
void UHoldPointComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}
