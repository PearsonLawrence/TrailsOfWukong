// All Rights Reserved by Punk Mage Games

#include "ClimbingComponent.h"
#include "SunWukong.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetSystemLibrary.h"

#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Public/WorldCollision.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "WukongAnimationInstance.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"

#include "PhysicsEngine/PhysicsAsset.h"
// Sets default values for this component's properties
UClimbingComponent::UClimbingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	LeftArrow = CreateDefaultSubobject<UArrowComponent>(FName("LeftArrow"));
	LeftArrow->bEditableWhenInherited = true;
	LeftArrow->SetRelativeLocation(FVector(50, -60, 40));
	LeftArrow->SetMobility(EComponentMobility::Movable);
	//LeftArrow->AttachTo(GetOwner()->GetRootComponent());

	RightArrow = CreateDefaultSubobject<UArrowComponent>(FName("RightArrow"));
	RightArrow->bEditableWhenInherited = true;
	RightArrow->SetRelativeLocation(FVector(50, 60, 40));
	RightArrow->SetMobility(EComponentMobility::Movable);
	//RightArrow->AttachTo(GetOwner()->GetRootComponent());

	LeftEdge = CreateDefaultSubobject<UArrowComponent>(FName("LeftEdge"));
	LeftEdge->bEditableWhenInherited = true;
	LeftEdge->SetRelativeLocation(FVector(75, -160, 40));
	LeftEdge->SetMobility(EComponentMobility::Movable);
	//LeftEdge->AttachTo(GetOwner()->GetRootComponent());

	RightEdge = CreateDefaultSubobject<UArrowComponent>(FName("RightEdge"));
	RightEdge->bEditableWhenInherited = true;
	RightEdge->SetRelativeLocation(FVector(75, 160, 40));
	RightEdge->SetMobility(EComponentMobility::Movable);
	//RightEdge->AttachTo(GetOwner()->GetRootComponent());

	UpArrow = CreateDefaultSubobject<UArrowComponent>(FName("UpArrow"));
	UpArrow->bEditableWhenInherited = true;
	UpArrow->SetRelativeLocation(FVector(65, 0, 260));
	UpArrow->SetMobility(EComponentMobility::Movable);
	//UpArrow->AttachTo(GetOwner()->GetRootComponent());

	ToHigh = CreateDefaultSubobject<UArrowComponent>(FName("ToHigh"));
	ToHigh->bEditableWhenInherited = true;
	ToHigh->SetRelativeLocation(FVector(65, 0, 360));
	ToHigh->SetMobility(EComponentMobility::Movable);
	//ToHigh->AttachTo(GetOwner()->GetRootComponent());

	CornerTraceLeft = CreateDefaultSubobject<UArrowComponent>(FName("CornerTraceLeft"));
	CornerTraceLeft->bEditableWhenInherited = true;
	CornerTraceLeft->SetRelativeLocation(FVector(0, -80, 40));
	CornerTraceLeft->SetMobility(EComponentMobility::Movable);
	//CornerTraceLeft->AttachTo(GetOwner()->GetRootComponent());

	CornerTraceRight = CreateDefaultSubobject<UArrowComponent>(FName("CornerTraceRight"));
	CornerTraceRight->bEditableWhenInherited = true;
	CornerTraceRight->SetRelativeLocation(FVector(0, 80, 40));
	CornerTraceRight->SetMobility(EComponentMobility::Movable);
	//CornerTraceRight->AttachTo(GetOwner()->GetRootComponent());


	bHipHang = false;
	// ...
}

void UClimbingComponent::ResetArrows()
{
	CornerTraceRight->SetMobility(EComponentMobility::Movable);
	CornerTraceLeft->SetMobility(EComponentMobility::Movable);
	ToHigh->SetMobility(EComponentMobility::Movable);
	UpArrow->SetMobility(EComponentMobility::Movable);
	RightEdge->SetMobility(EComponentMobility::Movable);
	LeftEdge->SetMobility(EComponentMobility::Movable);
	RightArrow->SetMobility(EComponentMobility::Movable);
	LeftArrow->SetMobility(EComponentMobility::Movable);
	LeftArrow->SetRelativeLocation(FVector(50, -60, 40));
	RightArrow->SetRelativeLocation(FVector(50, 60, 40));
	LeftEdge->SetRelativeLocation(FVector(50, -160, 40));
	RightEdge->SetRelativeLocation(FVector(50, 160, 40));
	UpArrow->SetRelativeLocation(FVector(65, 0, 260));
	ToHigh->SetRelativeLocation(FVector(65, 0, 360));
	CornerTraceLeft->SetRelativeLocation(FVector(0, -80, 40));
	CornerTraceRight->SetRelativeLocation(FVector(0, 80, 40));

	owner = Cast<ASunWukong>(GetOwner());
	Anim = Cast<UWukongAnimationInstance>(owner->GetMesh()->GetAnimInstance());

	CurrentState = EClimbStates::IdleTraceState;
}
// Called when the game starts
void UClimbingComponent::BeginPlay()
{
	Super::BeginPlay();
	
	ResetArrows();
}


// Called every frame
void UClimbingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	switch (CurrentState)
	{

		case EClimbStates::IdleTraceState:
			doIdle();
			break;
		case EClimbStates::ClimbUpState:
			doClimbUp();
			break;
		case EClimbStates::LedgeJumpState:
			doLedgeJump();
			break;
		case EClimbStates::CornerTurnState:
			doCornerTurn();
			break;
		case EClimbStates::JumpUpState:
			doJumpUp();
			break;
		case EClimbStates::HangingLedgeState:
			doHangingLedge();
			break;

	}
		
		JumpDelay -= GetWorld()->DeltaTimeSeconds;
		bIsJumpingBack = (JumpDelay > 0) ? true : false;

		
}

void UClimbingComponent::doHipTrace()
{
	FHitResult hit;
	
	FVector Project = owner->GetActorForwardVector() * FVector(40, 40, 0);

	FCollisionShape Sphere;
	
	Sphere.SetSphere(20);
	
	FColor Trace;
	if (GetWorld()->SweepSingleByChannel(hit, owner->GetActorLocation(), Project + owner->GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_Visibility, Sphere))
	{
		
		Trace = FColor::Green;
		if (bHipHang)
		{
			bHipHang = false;
			owner->GetMesh()->SetPhysicsAsset(TightHips);
			owner->GetMesh()->SetSimulatePhysics(false);
		}
	}
	else
	{
		
		if (!bHipHang)
		{

			Trace = FColor::Red;
			bHipHang = true; owner->GetMesh()->SetPhysicsAsset(LooseHips);
			owner->GetMesh()->SetSimulatePhysics(false);
		}
	}
	//DrawDebugLine(GetWorld(), owner->GetActorLocation(), Project + owner->GetActorLocation(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::doJumpUpTracer()
{
	
		TArray<FHitResult> hit;
		TArray<FHitResult> hit2;

		FVector Project = owner->GetActorForwardVector() * 15;
		FVector TempLocation = Project + FVector(0, 0, 40);
		FCollisionShape Capsule;

		Capsule.SetCapsule(1, 100);

		FCollisionShape Capsule2;

		Capsule2.SetCapsule(10, 50);

		
		FColor Trace;
		
		if (GetWorld()->SweepMultiByChannel(hit, UpArrow->GetComponentLocation(), UpArrow->GetComponentLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Capsule))
		{
			Trace = FColor::Green;
			bCanJumpUp = true;
		}
		else
		{
			Trace = FColor::Red;
			bCanJumpUp = false;
		}

		if (GetWorld()->SweepMultiByChannel(hit2, ToHigh->GetComponentLocation(), ToHigh->GetComponentLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Capsule2))
		{
			bJumpUpBlocked = true;
		}
		else
		{
			bJumpUpBlocked = false;
		}

		//DrawDebugCylinder(GetWorld(), UpArrow->GetComponentLocation(), Project, 20, 20, Trace, false, (-1.0f), (uint8)'\000', 2.f);
		////DrawDebugLine(GetWorld(), owner->GetActorLocation(), Project + owner->GetActorLocation(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::doForwardTracer()
{
	FHitResult hit;

	FVector Start = owner->GetActorLocation() + FVector(0, 0, owner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
	FVector Project = owner->GetActorForwardVector() * FVector(200, 200, 1) ;

	FCollisionShape Sphere;

	Sphere.SetSphere(20);
	FColor Trace;

	if (GetWorld()->SweepSingleByChannel(hit, Start, Project + Start,
		FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Sphere))
	{
		Trace = FColor::Green;
		WallLocation = hit.Location;
		WallNormal = hit.Normal;
		AActor* HitActor = hit.GetActor();
		FirstActor = HitActor;

		FVector Rot = FVector(0, 0, HitActor->GetActorRotation().Vector().Z);

		CurrentRot = Rot;
	}
	else
	{
		WallLocation = owner->GetActorLocation();
		WallNormal = owner->GetActorForwardVector() * -1;
		AActor* HitActor = nullptr;
		FirstActor = HitActor;

		CurrentRot = owner->GetActorRotation().Vector();
		Trace = FColor::Red;
	}

	//DrawDebugCylinder(GetWorld(), Start, Project + Start, 20, 20, Trace, false, (-1.0f), (uint8)'\000', 2.f);
	/*else
	{
		if (!bHipHang)
		{
			bHipHang = true;
		}
	}*/
}

void UClimbingComponent::doHeightTracer()
{
	FHitResult hit;
	FVector Start = owner->GetActorLocation() + FVector(0, 0, 130) + (owner->GetActorForwardVector() * 70);
	FVector Project = Start - FVector(0,0,130);

	FCollisionShape Sphere;

	Sphere.SetSphere(15);
	
	FColor Trace;
	if (GetWorld()->SweepSingleByChannel(hit, Start, Project , FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Sphere))
	{
		Trace = FColor::Green;
		if (FirstActor == hit.GetActor())
		{
			HeightLocation = hit.Location;

			bool falling = owner->GetCharacterMovement()->IsFalling();
			bool Accept = HipToLedge(Acceptance);
			if (Accept && !bIsClimbingLedge && !bIsHanging && falling && !bIsJumpingBack)
			{
					GrabLedge();
					if (CurrentState != EClimbStates::HangingLedgeState && CurrentState == EClimbStates::IdleTraceState)
					{
						CurrentState = EClimbStates::HangingLedgeState;
					}
			}
		}
	}
	else
	{

		Trace = FColor::Red;
		if (!bHipHang)
		{
			//bHipHang = true;

		}
	}
	//DrawDebugCylinder(GetWorld(), Start, Project, 15, 15, Trace,false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::GrabLedge()
{
	if (owner != nullptr && !bIsJumpingBack && !bIsJumping && !bTurningLeft && !bTurningRight)
	{
		UE_LOG(LogTemp, Warning, TEXT("move"));
		FVector vel = owner->GetVelocity();
		Anim->Execute_CanGrab(owner,true, vel);
		auto thing = owner->GetCharacterMovement();
		if (thing != nullptr)
		{
			owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
		}
		else
		{

			UE_LOG(LogTemp, Warning, TEXT("MovementNull"));
		}
		bIsHanging = true;
		FLatentActionInfo info = FLatentActionInfo();
		info.CallbackTarget = owner;
		UKismetSystemLibrary::MoveComponentTo(owner->GetCapsuleComponent(), MoveToLocation(), FRotator(0, WallNormal.Rotation().Yaw - 180,0),
			false, false, .13, true, EMoveComponentAction::Move, info);
		//owner->SetActorLocation(FMath::VInterpTo(owner->GetCapsuleComponent()->GetComponentLocation(), MoveToLocation(), GetWorld()->DeltaTimeSeconds, 5.f));
		owner->GetCharacterMovement()->StopMovementImmediately();
	}
	//TODO: Check to see if works problem may be here
}

bool UClimbingComponent::HipToLedge(float Accept)
{
	
	FVector Start = owner->GetMesh()->GetSocketLocation(FName("Slave_C_spine1"));
	float val = Start.Z - HeightLocation.Z;
	return (val >= Accept * -1 && val <= Accept);
}

FVector UClimbingComponent::MoveToLocation()
{
	FVector Val1 = WallNormal * FVector(10, 10, 0);

	return FVector(Val1.X + WallLocation.X, Val1.Y + WallLocation.Y, HeightLocation.Z - 117);
}

void UClimbingComponent::doLeftTracer()
{

	TArray<FHitResult> hit;
	TArray<FHitResult> hit2;
	FVector Project = owner->GetActorForwardVector() * FVector(40, 40, 0);

	FCollisionShape Capsule;

	Capsule.SetCapsule(25, 90);

	FCollisionShape Capsule2;

	Capsule2.SetCapsule(25, 90);

	FColor Trace, Trace2;

	if (GetWorld()->SweepMultiByChannel(hit, LeftArrow->GetComponentLocation(), LeftArrow->GetComponentLocation(), owner->GetActorRotation().Quaternion(), ECollisionChannel::ECC_GameTraceChannel1, Capsule))
	{
		//GrabLedge();
		Trace = FColor::Green;

		if (GetWorld()->SweepMultiByChannel(hit2, CornerTraceLeft->GetComponentLocation(), CornerTraceLeft->GetComponentLocation(), owner->GetActorRotation().Quaternion(), ECollisionChannel::ECC_GameTraceChannel1, Capsule2))
		{
			bCornerTurnLeft = true;
			Trace2 = FColor::Green;

		}
		else
		{
			bCornerTurnLeft = false;
			Trace2 = FColor::Red;
			/*if (bCornerTurnLeft)
			{
				bCornerTurnLeft = false;
				GrabLedge();
			}
			else
			{
				bCornerTurnLeft = false;
			}*/
		}
		if (!bTurningLeft && !bTurningRight)
		{
			bCanMoveLeft = true;
		}
	}
	else
	{
		Trace = FColor::Red;
		bCanMoveLeft = false;
	}
	//DrawDebugCapsule(GetWorld(), LeftArrow->GetComponentLocation(), 90, 25, owner->GetActorRotation().Quaternion(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
	//DrawDebugCapsule(GetWorld(), CornerTraceLeft->GetComponentLocation(), 90, 25, owner->GetActorRotation().Quaternion(), Trace2, false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::doRightTracer()
{
	TArray<FHitResult> hit;
	TArray<FHitResult> hit2;
	FVector Project = owner->GetActorForwardVector() * FVector(40, 40, 0);

	FCollisionShape Capsule;

	Capsule.SetCapsule(25, 90);

	FCollisionShape Capsule2;

	Capsule2.SetCapsule(25, 90);

	FColor Trace, Trace2;

	if (GetWorld()->SweepMultiByChannel(hit, RightArrow->GetComponentLocation(), RightArrow->GetComponentLocation(), owner->GetActorRotation().Quaternion(), ECollisionChannel::ECC_GameTraceChannel1, Capsule))
	{
		//GrabLedge();
		Trace = FColor::Green;
		if (GetWorld()->SweepMultiByChannel(hit2, CornerTraceRight->GetComponentLocation(), CornerTraceRight->GetComponentLocation(), owner->GetActorRotation().Quaternion(), ECollisionChannel::ECC_GameTraceChannel1, Capsule2))
		{
			bCornerTurnRight = true;
			Trace2 = FColor::Green;
		}
		else
		{
			Trace2 = FColor::Red;
			
			/*if (bCornerTurnRight)
			{
				bCornerTurnRight = false;
				GrabLedge();
			}*/
			bCornerTurnRight = false;
		}
		if (!bTurningLeft && !bTurningRight)
		{
			bCanMoveRight = true;
		}
	}
	else
	{
		Trace = FColor::Red;
		bCanMoveRight = false;
	}
	//DrawDebugCapsule(GetWorld(), RightArrow->GetComponentLocation(), 90, 25, owner->GetActorRotation().Quaternion(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
	//DrawDebugCapsule(GetWorld(), CornerTraceRight->GetComponentLocation(), 90, 25, owner->GetActorRotation().Quaternion(), Trace2, false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::MoveLedge()
{

	if (!bTurnedBack && bIsHanging && (bCanMoveLeft || bCanMoveRight))
	{
		doRightTracer();
		doLeftTracer();

		if (!bCornerTurnRight && owner->MovementRight > 0 || !bCornerTurnLeft && owner->MovementRight < 0)
		{
			Execute_MoveLeftRight(owner, owner->MovementRight);
		}
		MoveOnLedge();
		if (!bTurningLeft && !bTurningRight)
		{
			GrabLedge();
		}
	}
	else
	{
		bMovingRight = false;
		bMovingLeft = false;
	}
}

void UClimbingComponent::TurnBackLedge()
{

}

void UClimbingComponent::TurnForwardLedge()
{

}

void UClimbingComponent::JumpBackLedge()
{

}

void UClimbingComponent::JumpEdgeTraceLeft()
{

	TArray<FHitResult> hit;

	FVector Project = owner->GetActorForwardVector() * FVector(40, 40, 0);

	FCollisionShape Capsule;

	Capsule.SetCapsule(20, 90);
	FColor Trace;
	if (!bCornerTurnLeft)
	{
		if (GetWorld()->SweepMultiByChannel(hit, LeftEdge->GetComponentLocation(), LeftEdge->GetComponentLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Capsule))
		{
			bCanJumpLeft = (bIsJumping) ? false : true;
			Trace = FColor::Green;
		}
		else
		{
			Trace = FColor::Red;
			bCanJumpLeft = false;
		}

		//DrawDebugCapsule(GetWorld(), LeftEdge->GetComponentLocation(), 90, 20, owner->GetActorRotation().Quaternion(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
	}
}

void UClimbingComponent::TurningLeftTracer()
{
	FHitResult hit;

	FVector Project = LeftArrow->GetComponentLocation() + FVector(0, 0, 50);

	FCollisionShape Sphere;

	Sphere.SetSphere(20);
	
	if (GetWorld()->SweepSingleByChannel(hit, Project, (LeftArrow->GetForwardVector() * 40) + Project, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Sphere))
	{
		bCanTurnLeft = false;
	}
	else
	{
		bCanTurnLeft = (bIsJumping) ? false : true;
	}
}

void UClimbingComponent::JumpEdgeTraceRight()
{
	TArray<FHitResult> hit;

	FVector Project = owner->GetActorForwardVector() * FVector(40, 40, 0);

	FCollisionShape Capsule;

	Capsule.SetCapsule(20, 90);

	FColor Trace;
	if (!bCornerTurnRight)
	{
		if (GetWorld()->SweepMultiByChannel(hit, RightEdge->GetComponentLocation(), RightEdge->GetComponentLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Capsule))
		{
			bCanJumpRight = (bIsJumping) ? false : true;
			Trace = FColor::Green;
		}
		else
		{
			bCanJumpRight = false;
			Trace = FColor::Red;
		}

		//DrawDebugCapsule(GetWorld(), RightEdge->GetComponentLocation(), 90, 20, owner->GetActorRotation().Quaternion(), Trace, false, (-1.0f), (uint8)'\000', 2.f);
	}
}

void UClimbingComponent::TurningRightTracer()
{
	FHitResult hit;

	FVector Project = RightArrow->GetComponentLocation() + FVector(0, 0, 50);

	FCollisionShape Sphere;

	Sphere.SetSphere(20);

	FColor Trace;
	if (GetWorld()->SweepSingleByChannel(hit, Project, (RightArrow->GetForwardVector() * 40) + Project, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, Sphere))
	{
		bCanTurnRight = false;
		Trace = FColor::Green;
	}
	else
	{
		bCanTurnRight = (bIsJumping) ? false : true;

		Trace = FColor::Red;
	}
	//DrawDebugSphere(GetWorld(), Project, 20, 20, Trace, false, (-1.0f), (uint8)'\000', 2.f);
}

void UClimbingComponent::EndLedge()
{
	owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);

	if (Cast<ICanGrab>(owner->GetMesh()->GetAnimInstance()))
	{
		Cast<ICanGrab>(owner->GetMesh()->GetAnimInstance())->Execute_CanGrab(owner,false, FVector(0, 0, 0));
	}
	bIsHanging = false;
	
	bHipHang = false; 
	/*owner->GetMesh()->SetPhysicsAsset(TightHips);
	owner->GetMesh()->SetSimulatePhysics(false);
*/
	CurrentState = EClimbStates::IdleTraceState;
	// set Physics asset
	//owner->GetMesh()->SetPhysicsAsset();
	owner->GetMesh()->SetSimulatePhysics(false);
}

void UClimbingComponent::JumpRightLedge()
{

	if (owner->MovementRight > 0 && !bIsJumping)
	{
		
		
		owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);

		
		owner->Execute_JumpRight(owner, true);
		bIsJumping = true;
		bIsHanging = true;
		
		//owner->DisableInput(GetWorld()->GetFirstPlayerController());
		FTimerHandle handle = FTimerHandle();
		//GetWorld()->GetTimerManager().SetTimer(handle, 0.0f, false, 1.5f);
		
		if (bCanJumpUp)
		{
			owner->EnableInput(GetWorld()->GetFirstPlayerController());
		}
		else
		{
			GrabLedge();
		}

	}
	
}

void UClimbingComponent::JumpLeftLedge()
{

	if (owner->MovementRight < 0 && !bIsJumping && Cast<ICanGrab>(owner->GetMesh()->GetAnimInstance()))
	{
		owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
		
		Anim->Execute_JumpLeft(owner, true);
		owner->Execute_JumpLeft(owner, true);
		bIsJumping = true;
		bIsHanging = true;

		//owner->DisableInput(GetWorld()->GetFirstPlayerController());
		FTimerHandle handle = FTimerHandle();
		GetWorld()->GetTimerManager().SetTimer(handle, 0.0f, false, 1.5f);

		if (bCanJumpUp)
		{

			owner->EnableInput(GetWorld()->GetFirstPlayerController());

		}
		else
		{
			GrabLedge();
		}

	}

}

void UClimbingComponent::JumpUpLedge()
{

	if (owner->MovementRight == 0 && !bIsJumping && bCanJumpUp)
	{
		owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);

		Anim->Execute_JumpUp(owner, true);
		owner->Execute_JumpUp(owner, true);
		bIsJumping = true;
		bIsHanging = true;

		//owner->DisableInput(GetWorld()->GetFirstPlayerController());
		FTimerHandle handle = FTimerHandle();
		GetWorld()->GetTimerManager().SetTimer(handle, 0.0f, false, 1.5f);

		if (bCanJumpUp)
		{

			owner->EnableInput(GetWorld()->GetFirstPlayerController());

		}
		else
		{
			GrabLedge();
		}

	}

}

void UClimbingComponent::ClimbUpLedge()
{
	FHitResult hit;
	FVector Offset = owner->GetActorForwardVector() * 50;
	FVector Start = owner->GetActorLocation() + FVector(Offset.X, Offset.Y, Offset.Z + 150);

	FCollisionShape Sphere;

	Sphere.SetSphere(30);

	FColor Temp = FColor::Red;
	if (!bIsClimbingLedge && !bCanJumpUp && !GetWorld()->SweepSingleByChannel(hit, Start, Start, FQuat::Identity, ECollisionChannel::ECC_Camera, Sphere))
	{
		Temp = FColor::Green;
		owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);

		owner->Execute_ClimbLedge(owner, true);
		
		bIsClimbingLedge = true;
		bIsHanging = false;
	}
	//DrawDebugSphere(GetWorld(), Start, 30, 30, Temp, false, 1.0f, (uint8)'\000',1);
}

void UClimbingComponent::MoveOnLedge()
{
	float DT = GetWorld()->DeltaTimeSeconds;
	
		if (bCanMoveRight && !bCornerTurnRight)
		{
			if (owner->MovementRight > 0)
			{
				owner->SetActorLocation(FMath::VInterpTo(owner->GetActorLocation(), (owner->GetActorRightVector() * 20) + owner->GetActorLocation(), DT, 5));
				bMovingRight = true;
				bMovingLeft = false;

				if (bCornerTurnRight)
				{

					FRotator ToRot = owner->GetControlRotation() + FRotator(0, 90, 0);

					owner->GetController()->SetControlRotation(FMath::RInterpTo(owner->GetControlRotation(), ToRot, DT, 1.5f));

					/*if (owner->MovementRight < 0 && !bCornerTurnLeft)
					{
					FRotator ToRot = owner->GetControlRotation() + FRotator(0, -90, 0);

					owner->GetController()->SetControlRotation(FMath::RInterpTo(owner->GetControlRotation(), ToRot, DT, 1.5f));

					}*/
				}
				else if (!bIsJumping && !bCornerTurnLeft)
				{
					//GrabLedge();
				}
			}

		}

		if (bCanMoveLeft && !bCornerTurnLeft)
		{

			if (owner->MovementRight < 0)
			{
				owner->SetActorLocation(FMath::VInterpTo(owner->GetActorLocation(), (owner->GetActorRightVector() * -20) + owner->GetActorLocation(), DT, 5));
				bMovingRight = false;
				bMovingLeft = true;

				if (bCornerTurnLeft)
				{

					FRotator ToRot = owner->GetControlRotation() + FRotator(0, -90, 0);

					owner->GetController()->SetControlRotation(FMath::RInterpTo(owner->GetControlRotation(), ToRot, DT, 1.5f));


					/*if (owner->MovementRight > 0 && !bCornerTurnRight)
					{
					FRotator ToRot = owner->GetControlRotation() + FRotator(0, 90, 0);

					owner->GetController()->SetControlRotation(FMath::RInterpTo(owner->GetControlRotation(), ToRot, DT, 1.5f));

					}*/
				}
				else if (!bIsJumping && !bCornerTurnRight)
				{
					//GrabLedge();
				}

			}




		}

		if (owner->MovementRight == 0)
		{
			bMovingLeft = false;
			bMovingRight = false;

		}
	
}

void UClimbingComponent::doIdle()
{
	if (!bIsJumping)
	{
		(bTurnedBack) ? Acceptance = 50 : Acceptance = 125; //50 , 125
		doForwardTracer();
		doHeightTracer();
		//UE_LOG(LogTemp, Warning, TEXT("wow"));
	}
}

void UClimbingComponent::doClimbUp()
{

}

void UClimbingComponent::doLedgeJump()
{

}

void UClimbingComponent::doCornerTurn()
{

}

void UClimbingComponent::doJumpUp()
{

}

void UClimbingComponent::doHangingLedge()
{
	if (bIsHanging)
	{
		bHardLanding = false;
		doHipTrace();

		if (!bIsJumping)
		{
			(bTurnedBack) ? Acceptance = 50 : Acceptance = 125; //50 , 125
			doRightTracer();
			doLeftTracer();
			MoveLedge();
			TurnBackLedge();
			TurnForwardLedge();
			JumpBackLedge();
		}

		doForwardTracer();
		doHeightTracer();
		doJumpUpTracer();

		if (bCanMoveLeft)
		{
			bCanJumpLeft = false;
			bCanTurnLeft = false;
		}
		else
		{
			if (!bCornerTurnLeft)
			{
				JumpEdgeTraceLeft();
			}
			TurningLeftTracer();
		}
		if (bCanMoveRight)
		{
			bCanJumpRight = false;
			bCanTurnRight = false;
		}
		else
		{
			if (!bCornerTurnRight)
			{
				JumpEdgeTraceRight();
			}
			TurningRightTracer();
		}
	}
}
