// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "WeaponMeshComponent.generated.h"

/**
 * 
 */
UCLASS()
class WUKONG_API UWeaponMeshComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
	bool CanDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
	float WeaponDamageAmount;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
	float HeavyDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
	float LightDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
	bool HeavyAttack;
		
	//
};
