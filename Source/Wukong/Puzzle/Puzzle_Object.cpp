// All Rights Reserved by Punk Mage Games

#include "Puzzle_Object.h"
#include "Puzzle/HoldPointComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Puzzle/PuzzlePieceAttachPointComponent.h"


// Sets default values
APuzzle_Object::APuzzle_Object()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;





	//Creating the mesh component and attaching it to the root component
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->AttachTo(RootComponent);


	//Creating hold point components on the object then attaching them to the mesh
	HoldPointComponent = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent"));
	HoldPointComponent->AttachTo(StaticMeshComponent);
	HoldPointComponent2 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent2"));
	HoldPointComponent2->AttachTo(StaticMeshComponent);
	HoldPointComponent3 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent3"));
	HoldPointComponent3->AttachTo(StaticMeshComponent);
	HoldPointComponent4 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent4"));
	HoldPointComponent4->AttachTo(StaticMeshComponent);
	HoldPointComponent5 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent5"));
	HoldPointComponent5->AttachTo(StaticMeshComponent);
	HoldPointComponent6 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent6"));
	HoldPointComponent6->AttachTo(StaticMeshComponent);
	HoldPointComponent7 = CreateDefaultSubobject<UHoldPointComponent>(TEXT("HoldPointComponent7"));
	HoldPointComponent7->AttachTo(StaticMeshComponent);


}

// Called when the game starts or when spawned
void APuzzle_Object::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APuzzle_Object::Tick(float DeltaTime)
{

}