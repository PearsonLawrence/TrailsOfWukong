// All Rights Reserved by Punk Mage Games

#include "BaseFishAIController.h"
#include "AIBaseCharacter.h"
#include "Engine/World.h"
#include "AI/Navigation/NavigationSystem.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AITargetPoint.h"
#include "Navigation/CrowdFollowingComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
ABaseFishAIController::ABaseFishAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
	
	CurrentState = EFishAIStates::IdleState;
	AcceptanceRadius = 100;
	RoamRadius = 500;
	WaitTime = 0;
}

void ABaseFishAIController::BeginPlay()
{
	Super::BeginPlay();
	TargetActor = GetWorld()->GetFirstPlayerController()->GetPawn();
	ThisPawn = GetPawn();
	thisCharacter = Cast<AAIBaseCharacter>(GetCharacter());
	SetNewRoamPoint(MoveLocation, RoamRadius);
	IsGuard = thisCharacter->IsGuard;
	IsLeader = thisCharacter->IsLeader;
	if (IsGuard)
	{
		GetGuardTargetPoints();
	}
	else 
	{
		GetCivilianTargetPoints();
	}
}

void ABaseFishAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossesedAI = Cast<AAIBaseCharacter>(InPawn);
		if (!ensure(PossesedAI)) { return; }
	}
}
	
void ABaseFishAIController::GetCivilianTargetPoints()
{
	TArray<AActor*> Temp;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), thisCharacter->PointBP, Temp);
	for (int i = 0; i < Temp.Num(); i++)
	{
		AAITargetPoint* tempPoint = Cast<AAITargetPoint>(Temp[i]);
		if (tempPoint->GuardTargetPoint == false && !tempPoint->CowerPoint)
		{
			thisCharacter->TargetPoints.Add(tempPoint);
		}
		else if (tempPoint->CowerPoint)
		{
			thisCharacter->CowerPoints.Add(tempPoint);
		}
	}
}

void ABaseFishAIController::GetGuardTargetPoints()
{
	TArray<AActor*> Temp;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), thisCharacter->PointBP, Temp);
	for (int i = 0; i < Temp.Num(); i++)
	{
		AAITargetPoint* tempPoint = Cast<AAITargetPoint>(Temp[i]);
		if (tempPoint->GuardTargetPoint)
		{
			thisCharacter->TargetPoints.Add(tempPoint);
		}
	}
}

void ABaseFishAIController::OnPossesedAIDeath()
{
	if (!GetPawn()) { return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}

void ABaseFishAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (!ensure(TargetActor && ThisPawn)) { return; }

	switch (CurrentState)
	{
		case EFishAIStates::IdleState:
			doIdle(DeltaTime);
			break;
		case EFishAIStates::TrackState:
			doTrack();
			break;
		case EFishAIStates::CombatState:
			doCombat(DeltaTime);
			break;
		case EFishAIStates::AttackState:
			doAttack();
			break;
		case EFishAIStates::CowerState:
			doCower();
			break;
	}
}

void ABaseFishAIController::doIdle(float DT)
{
	WaitTime -= DT;

	if (WaitTime <= 0)
	{
		bool squadmember = (IsGuard && !IsLeader);
		MoveLocation = (IsGuard && !IsLeader && thisCharacter->SquadLeader != nullptr) ? thisCharacter->SquadPoint->GetComponentLocation() : MoveLocation;
		MoveToLocation(MoveLocation, AcceptanceRadius, squadmember, true,false, false);
		CurrentDecisionState = EDecisionState::RoamState;
		
	}

	switch (CurrentDecisionState)
	{
	case EDecisionState::HideState:
		//doCowerDecision();
		break;
	case EDecisionState::StoreState:
		if(thisCharacter->CurrentPoint != nullptr)
		thisCharacter->SetActorRotation(FMath::RInterpTo(thisCharacter->GetActorRotation(), thisCharacter->CurrentPoint->GetActorRotation(), GetWorld()->DeltaTimeSeconds, 5));
		break;
	case EDecisionState::ChaseState:
		//doChaseDecision();
		break;
	}
}

void ABaseFishAIController::doTrack()
{

}

void ABaseFishAIController::doCombat(float DT)
{

	WaitTime -= DT;

	if (!Attacking)
	{
		MoveLocation = thisCharacter->AttackPoint->GetActorLocation();
		MoveToLocation(MoveLocation, 50, true, true, false, false);
	}

	switch (Attacking)
	{
	case true:

		break;
	case false:

		break;
	}
}

void ABaseFishAIController::doAttack()
{
	if (!Attacking)
	{
		thisCharacter->Execute_doLightAttack(thisCharacter, 0);
		Attacking = true;
	}

}

void ABaseFishAIController::doCower()
{
	//MoveLocation 
	
	switch (CurrentDecisionState)
	{
	case EDecisionState::HideState:
		if (thisCharacter->CurrentPoint != nullptr)
			thisCharacter->SetActorRotation(FMath::RInterpTo(thisCharacter->GetActorRotation(), thisCharacter->CurrentPoint->GetActorRotation(), GetWorld()->DeltaTimeSeconds, 5));
		break;
	}

	if (WaitTime <= 0 && CurrentDecisionState == EDecisionState::HideState)
	{
		CurrentDecisionState = EDecisionState::RoamState;

		MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
		//TODO: call anim event here
	}
	else if (WaitTime <= 0 && CurrentDecisionState == EDecisionState::RoamState)
	{
		MoveToLocation(MoveLocation, AcceptanceRadius, true, true, false, false);
	}
}

void ABaseFishAIController::DoChase(AActor* target)
{
	CurrentState = EFishAIStates::CombatState;
	
	thisCharacter->AttackPoint = target;
	MoveLocation = thisCharacter->AttackPoint->GetActorLocation();
	WaitTime = 0;

	CurrentDecisionState = EDecisionState::ChaseState;
	thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->GetCharacterMovement()->MaxWalkSpeed * 2;
	thisCharacter->InDanger = true;

	
}


void ABaseFishAIController::MakeDecision()
{
	switch (CurrentDecisionState)
	{
	case EDecisionState::RoamState:
		doIdleDecision();
		break;
	case EDecisionState::HideState:
		doCowerDecision();
		break;
	case EDecisionState::ChatState:
		doChatDecision();
		break;
	case EDecisionState::StoreState:
		doStoreDecision();
		break;
	case EDecisionState::ChaseState:
		doChaseDecision();
		break;
	}
}

void ABaseFishAIController::SetNewRoamPoint(FVector &NewPoint, float radius)
{
	NewPoint = UNavigationSystem::GetRandomPointInNavigableRadius(GetWorld(), thisCharacter->GetActorLocation(), radius);
}

void ABaseFishAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	if (Result.Code == EPathFollowingResult::Success)
	{
		MakeDecision();
	}
	else if (Result.Code == EPathFollowingResult::Blocked)
	{

	}
}

void ABaseFishAIController::doIdleDecision()
{
	if (IsGuard && !IsLeader)
	{
		IdleGuardDecision();
	}
	else
	{
		IdleCivilianDecision();
	}
}

void ABaseFishAIController::doChaseDecision()
{
	if (!Attacking)
	{
		doAttack();
	}
}

void ABaseFishAIController::doStoreDecision()
{

}

void ABaseFishAIController::doCowerDecision()
{
	if (thisCharacter->InDanger)
	{
		int32 RandomPoint = FMath::RandRange(0, thisCharacter->CowerPoints.Num() - 1);
		if (thisCharacter->CowerPoints.Num() > 0)
		{
			if (thisCharacter->CowerPoints[RandomPoint] != nullptr)
			{
				WaitTime = (thisCharacter->CurrentPoint != nullptr) ? thisCharacter->CurrentPoint->PointWaitTime : WaitTime = FMath::RandRange(0.0f, 15.0f);

				MoveLocation = thisCharacter->TargetPoints[RandomPoint]->GetActorLocation();

				thisCharacter->CurrentPoint = thisCharacter->CowerPoints[RandomPoint];
			}
			else
			{
				SetNewRoamPoint(MoveLocation, RoamRadius);
				WaitTime = FMath::RandRange(0.0f, 15.0f);
				thisCharacter->CurrentPoint = nullptr;
			}
		}

		CurrentDecisionState = EDecisionState::HideState;
	}
	else
	{
		WaitTime = 0;
		CurrentState = EFishAIStates::IdleState;
		CurrentDecisionState = EDecisionState::RoamState;
		SetNewRoamPoint(MoveLocation, RoamRadius);

		thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->GetCharacterMovement()->MaxWalkSpeed / 2;
		thisCharacter->CurrentPoint = nullptr;
	}
}

void ABaseFishAIController::doChatDecision()
{

}

void ABaseFishAIController::IdleCivilianDecision()
{
	float Rand = FMath::RandRange(0.0f, 100.0f);

	float range = (IsGuard) ? 90.0f : 50.0f;
	if (Rand >= range)
	{
		SetNewRoamPoint(MoveLocation, RoamRadius);
		thisCharacter->CurrentPoint = nullptr;
		WaitTime = FMath::RandRange(0.0f, 15.0f);
	}
	else
	{
		int32 RandomPoint = FMath::RandRange(0, thisCharacter->TargetPoints.Num() - 1);
		if (thisCharacter->TargetPoints.Num() > 0)
		{
			if (thisCharacter->TargetPoints[RandomPoint] != nullptr)
			{
				WaitTime = (thisCharacter->CurrentPoint != nullptr) ? thisCharacter->CurrentPoint->PointWaitTime : WaitTime = FMath::RandRange(0.0f, 15.0f);

				MoveLocation = thisCharacter->TargetPoints[RandomPoint]->GetActorLocation();

				thisCharacter->CurrentPoint = thisCharacter->TargetPoints[RandomPoint];
				CurrentDecisionState = thisCharacter->CurrentPoint->StoredState;
			}
			else
			{
				SetNewRoamPoint(MoveLocation, RoamRadius);
				WaitTime = FMath::RandRange(0.0f, 15.0f);
				thisCharacter->CurrentPoint = nullptr;
			}
		}
	}
}
void ABaseFishAIController::IdleGuardDecision()
{
	if (thisCharacter->SquadLeader != nullptr && thisCharacter->SquadPoint != nullptr)
	{
		MoveLocation = thisCharacter->SquadPoint->GetComponentLocation();
		AcceptanceRadius = 5.0f;
	}
	else
	{
		IdleCivilianDecision();
		AcceptanceRadius = 100.0f;
	}
}

void ABaseFishAIController::DoActionCower()
{
	WaitTime = 0;
	CurrentState = EFishAIStates::CowerState;
	CurrentDecisionState = EDecisionState::HideState;
	thisCharacter->GetCharacterMovement()->MaxWalkSpeed = thisCharacter->GetCharacterMovement()->MaxWalkSpeed * 2;
	int32 RandomPoint = FMath::RandRange(0, thisCharacter->CowerPoints.Num() - 1);
	thisCharacter->InDanger = true;
	if (thisCharacter->CowerPoints.Num() > 0)
	{
		if (thisCharacter->CowerPoints[RandomPoint] != nullptr)
		{
			MoveLocation = thisCharacter->CowerPoints[RandomPoint]->GetActorLocation();

			thisCharacter->CurrentPoint = thisCharacter->CowerPoints[RandomPoint];
		}
		else
		{
			SetNewRoamPoint(MoveLocation, 1000);
			thisCharacter->CurrentPoint = nullptr;
		}
	}
	else
	{
		SetNewRoamPoint(MoveLocation, 1000);
		thisCharacter->CurrentPoint = nullptr;
	}
}

EFishAIStates ABaseFishAIController::GetCurrentState()
{
	return CurrentState;
}
void ABaseFishAIController::SetCurrentState(EFishAIStates State)
{
	CurrentState = State;
}