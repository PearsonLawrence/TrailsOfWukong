// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Attackable.h"
#include "Damageable.h"

#include "AIBaseCharacter.generated.h"
UCLASS()
class WUKONG_API AAIBaseCharacter : public ACharacter, public IAttackable, public IDamageable
{
	GENERATED_BODY()

private:
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;
public:	
	AAIBaseCharacter();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = reference)
	class ABaseFishAIController* myAIController;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class UPawnSensingComponent* PawnSensor;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UHealthComponent* HealthComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UCombatComponent* CombatComponent;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWeaponMeshComponent* Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class USceneComponent* GuardPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class USceneComponent* GuardPoint2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class USceneComponent* GuardPoint3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class USceneComponent* GuardPoint4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
	class USceneComponent* GuardPoint5;

	TArray<USceneComponent*> GuardPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targets)
	class APawn* SensedPawn;

	UFUNCTION()
	void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
	void OnSeePawn(APawn *OtherPawn);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
	bool IsGuard;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
	bool IsLeader;
	UPROPERTY(EditAnywhere, Category = "target")
	TArray<class AAIBaseCharacter*> Squad;

	UPROPERTY(EditAnywhere, Category = "target")
	class AAIBaseCharacter* SquadLeader;
	UPROPERTY(EditAnywhere, Category = "target")
	class USceneComponent* SquadPoint;
	UPROPERTY(EditAnywhere, Category = "target")
	class AActor* AttackPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
	bool InDanger;

	UPROPERTY(EditAnywhere, Category = "Turning") // consider EditDefaultsOnly
	float TurnRate;
	UPROPERTY(EditAnywhere, Category = "target")
	TArray<class AAITargetPoint*> TargetPoints;
	UPROPERTY(EditAnywhere, Category = "target")
	TArray<class AAITargetPoint*> CowerPoints;

	UPROPERTY(EditAnywhere, Category = "target")
	TSubclassOf<class AAITargetPoint> PointBP;

	UPROPERTY(EditAnywhere, Category = "target")
	class AAITargetPoint* CurrentPoint;


protected:
	void SetSquad();
};
