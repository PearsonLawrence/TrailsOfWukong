// All Rights Reserved by Punk Mage Games

#include "Puzzle_Piece.h"
#include "DrawDebugHelpers.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Puzzle/PuzzlePieceAttachPointComponent.h"


// Sets default values
APuzzle_Piece::APuzzle_Piece()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PuzzlePiece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	PuzzlePiece->AttachTo(RootComponent);


	PuzzlePieceAttachPointComponent = CreateDefaultSubobject<UPuzzlePieceAttachPointComponent>(TEXT("PuzzlePieceAttachPointComponent"));
	PuzzlePieceAttachPointComponent->AttachTo(PuzzlePiece);

}

// Called when the game starts or when spawned
void APuzzle_Piece::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APuzzle_Piece::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

