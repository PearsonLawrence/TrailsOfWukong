// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseFishAIController.generated.h"

/**
 * 
 */

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EFishAIStates : uint8
{
	IdleState 			UMETA(DisplayName = "IdleState"),
	TrackState 			UMETA(DisplayName = "TrackState"),
	AttackState 		UMETA(DisplayName = "AttackState"),
	CombatState 		UMETA(DisplayName = "CombatState"),
	CowerState 			UMETA(DisplayName = "CowerState")
};
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EDecisionState : uint8
{
	RoamState 		UMETA(DisplayName = "RoamState"),
	ChatState 		UMETA(DisplayName = "ChatState"),
	HideState 		UMETA(DisplayName = "HideState"),
	StoreState 		UMETA(DisplayName = "StoreState"),
	ChaseState 		UMETA(DisplayName = "ChaseState"),
};

UCLASS()
class WUKONG_API ABaseFishAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "target")
	EFishAIStates CurrentState;
	UPROPERTY(EditAnywhere, Category = "target")
	EDecisionState CurrentDecisionState;

	ABaseFishAIController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float AcceptanceRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float RoamRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool IsGuard;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool IsLeader;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
		bool Attacking;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		FVector MoveLocation;
	UPROPERTY(EditAnywhere, Category = "target")
		AActor* TargetActor;
	UPROPERTY(EditAnywhere, Category = "target")
		float WaitTime;

	UFUNCTION(BlueprintCallable)
		void DoActionCower();

	UFUNCTION(BlueprintCallable)
	void DoChase(class AActor* target);
	UFUNCTION(BlueprintCallable)
	EFishAIStates GetCurrentState();
	UFUNCTION(BlueprintCallable)
	void SetCurrentState(EFishAIStates State);

protected:
	

	class AAIBaseCharacter* thisCharacter;
	class APawn* ThisPawn;
private:

	virtual void SetPawn(APawn* InPawn) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result) override;

	UFUNCTION()
	void OnPossesedAIDeath();
	UFUNCTION()
	void SetNewRoamPoint(FVector &NewPoint, float radius);

	
	////////////// State Actions
	void doIdle(float DT);
	void doTrack();
	void doCombat(float DT);
	void doAttack();
	void doCower();
	
	////////////// State Decisions
	void doIdleDecision();
	void doChaseDecision();
	void doChatDecision();
	void doStoreDecision();
	void doCowerDecision();

	void IdleGuardDecision();
	void IdleCivilianDecision();


	void MakeDecision();

	void GetCivilianTargetPoints();
	void GetGuardTargetPoints();
};
