// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WukongGameMode.h"
#include "WukongCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWukongGameMode::AWukongGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/WukongContent/Programming/Pearson/PlayerContent/BluePrints/Wukong_BP"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
