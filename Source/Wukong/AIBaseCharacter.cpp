// All Rights Reserved by Punk Mage Games

#include "AIBaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "BaseFishAIController.h"
#include "Components/SceneComponent.h"
#include "SunWukong.h"
#include "HealthComponent.h"
#include "CombatComponent.h"
#include "WeaponMeshComponent.h"
// Sets default values
AAIBaseCharacter::AAIBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetCharacterMovement()->MaxWalkSpeed = 100.0f;
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(FName("PawnSensing"));
	PawnSensor->SensingInterval = .25f; // 4 times per second
	PawnSensor->bOnlySensePlayers = true;
	PawnSensor->SetPeripheralVisionAngle(85.f);
	TurnRate = 10.0f;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	HealthComponent->isPlayer = false;

	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("Combat Component"));
	Weapon = CreateDefaultSubobject<UWeaponMeshComponent>(TEXT("Staff"));
	Weapon->AttachTo(GetMesh(), FName("hand_r"));

	GuardPoint = CreateDefaultSubobject<USceneComponent>(FName("SquadPoint1"));
	GuardPoint->AttachTo(RootComponent);
	GuardPoint->SetMobility(EComponentMobility::Movable);

	GuardPoint2 = CreateDefaultSubobject<USceneComponent>(FName("SquadPoint2"));
	GuardPoint2->AttachTo(RootComponent);
	GuardPoint2->SetMobility(EComponentMobility::Movable);

	GuardPoint3 = CreateDefaultSubobject<USceneComponent>(FName("SquadPoint3"));
	GuardPoint3->AttachTo(RootComponent);
	GuardPoint3->SetMobility(EComponentMobility::Movable);

	GuardPoint4 = CreateDefaultSubobject<USceneComponent>(FName("SquadPoint4"));
	GuardPoint4->AttachTo(RootComponent);
	GuardPoint4->SetMobility(EComponentMobility::Movable);

	GuardPoint5 = CreateDefaultSubobject<USceneComponent>(FName("SquadPoint5"));
	GuardPoint5->AttachTo(RootComponent);
	GuardPoint5->SetMobility(EComponentMobility::Movable);

	GuardPoints.Add(GuardPoint);
	GuardPoints.Add(GuardPoint2);
	GuardPoints.Add(GuardPoint3);
	GuardPoints.Add(GuardPoint4);
	GuardPoints.Add(GuardPoint5);
}
void AAIBaseCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	PawnSensor->OnSeePawn.AddDynamic(this, &AAIBaseCharacter::OnSeePawn);
	PawnSensor->OnHearNoise.AddDynamic(this, &AAIBaseCharacter::OnHearNoise);
}
// Called when the game starts or when spawned
void AAIBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	myAIController = Cast<ABaseFishAIController>(GetController());
	if (IsLeader && IsGuard) SetSquad();


}
// Called every frame
void AAIBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (GetVelocity().Size() > 1)
	{
		FRotator Dir3 = FRotator(0, GetVelocity().Rotation().Yaw, 0);

		SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir3, GetWorld()->DeltaTimeSeconds, TurnRate));
		//myAIController->SetControlRotation();
	}
}
// Called to bind functionality to input
void AAIBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
void AAIBaseCharacter::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{
	const FString VolumeDesc = FString::Printf(TEXT(" at volume %f"), Volume);
	FString message = TEXT("Heard Actor ") + OtherActor->GetName() + VolumeDesc;
	UE_LOG(LogTemp, Warning, TEXT("%s"), *message);  
}///

void AAIBaseCharacter::OnSeePawn(APawn *OtherPawn)
{
	//if (SensedPawn != OtherPawn)
	//{
	//	ASunWukong* Sun = Cast<ASunWukong>(OtherPawn);
	//	if (Sun != nullptr)
	//	{
	//		//FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
	//		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
	//		SensedPawn = OtherPawn;
	//		if (Sun->isHostile && myAIController->CurrentState != EFishAIStates::CowerState)
	//		{
	//			myAIController->DoCower();
	//		}
	//	}
	//}
	//else 
	//{
	//	ASunWukong* Sun = Cast<ASunWukong>(SensedPawn);
	//	if (Sun->isHostile && myAIController->CurrentState != EFishAIStates::CowerState)
	//	{
	//		myAIController->DoCower();
	//	}
	//}
	//FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

	// TODO: game-specific logic
}
void AAIBaseCharacter::SetSquad()
{
	if (IsLeader && IsGuard)
	{
		for (int i = 0; i < Squad.Num(); i++)
		{
			if (Squad[i] != nullptr && GuardPoints[i] != nullptr)
			{
				Squad[i]->SquadPoint = GuardPoints[i];
				Squad[i]->SquadLeader = this;
			}
		}
	}
}
