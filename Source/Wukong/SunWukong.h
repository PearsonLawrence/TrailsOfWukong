// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CanGrab.h"
#include "Attackable.h"
#include "Damageable.h"
#include "SunWukong.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ECameraStates : uint8
{
	SideScrollerState 	    UMETA(DisplayName = "SideScrollerState"),
	LockedPoseandRotState 	UMETA(DisplayName = "LockedPosandRotState"),
	LockedPoseandLookState 	UMETA(DisplayName = "LockedPoseandLookState"),
	LockedPoseZState 		UMETA(DisplayName = "LockedPoseZState"),
	LookFollowState     	UMETA(DisplayName = "LookandFollowAtAngle"),
	PlayerControlCamera     UMETA(DisplayName = "PlayerControlCamera"),
	SpiralStairCaseState 	UMETA(DisplayName = "SpiralStairCaseState"),
	CinematicCameraState 	UMETA(DisplayName = "CinematicCameraState"),
	ChatCameraState 		UMETA(DisplayName = "ChatCameraState"),
	BossCamState 			UMETA(DisplayName = "BossCamState")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlayerStates : uint8
{
	IdleState 			    UMETA(DisplayName = "IdleState"),
	CombatState 			UMETA(DisplayName = "CombatState"),
	BossCombatState 		UMETA(DisplayName = "CombatState"),
	AttackState 			UMETA(DisplayName = "AttackState"),
	ChatState	 			UMETA(DisplayName = "ChatState"),
	DodgeState 				UMETA(DisplayName = "DodgeState")
};

UCLASS(config = Game)
class WUKONG_API ASunWukong : public ACharacter, public ICanGrab, public IAttackable, public IDamageable
{

	GENERATED_BODY()
	
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class ASideScrollerCamera* FollowCamera;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Climbing, meta = (AllowPrivateAccess = "true"))
	class UClimbingComponent* ClimbingComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UCombatComponent* CombatComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UHealthComponent* HealthComponent;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWeaponMeshComponent* Staff;
	UPROPERTY(Category = Quest, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UQuestComponent* QuestComponent;

	

public:
	ASunWukong();
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bInMoveableCinematic;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bInLockedCinematic;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* CamHoldPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Climbing, meta = (AllowPrivateAccess = "true"))
		class AActor* StoredActor;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool isHostile;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float SideRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float uprate;


	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float MovementSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float MovementRight;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float VelocityTurnSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool ForwardOpen;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool BackOpen;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool RightOpen;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool LeftOpen;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool Sprint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	bool inBossFight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Jumping)
	int MaxJumpCount;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = Jumping)
	int JumpCount;

	UPROPERTY(EditDefaultsOnly, Category = Movement)
	FVector MovementDirection;

	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float WalkSpeed;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Movement)
	float CinematicWalkSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float SprintSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float CombatWalkSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float CombatSprintSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float AttackSpeed;

	UFUNCTION(BlueprintCallable)
	void SetCamera(ASideScrollerCamera* Cam);
	UFUNCTION(BlueprintCallable)
	void SetCameraState(ECameraStates State, AActor* inputActor);

	UFUNCTION(BlueprintCallable)
		EPlayerStates GetCurrentState();
	UFUNCTION(BlueprintCallable)
		void SetCurrentState(EPlayerStates State);


	UPROPERTY(EditDefaultsOnly, Category = Camera)
	ECameraStates CameraState;

	UPROPERTY(EditAnywhere, Category = Player)
	EPlayerStates CurrentState;

	/////Input Values//////
	void MoveForward(float Value);
	void MoveRight(float Value);
	void PlayerJump();
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	void ShowQuest();
	void doSprint(float Value);
	void doDodge();
	void doLightAttack();
	void doHeavyAttack();
	void SwitchMode();
	///////////////////////



	void TurnLeft();
	void TurnRight();

	void doIdleJump();
	void doCombatJump();
	
	void doIdleMoveForward(float Value);
	void doCombatMoveForward(float Value);
	void doAttackMoveForward(float Value);

	void doIdleMoveRight(float Value);
	
	void doCombatMoveRight(float Value);
	void doAttackMoveRight(float Value);


	void doRoll();

	void Back();
	void Forward();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
