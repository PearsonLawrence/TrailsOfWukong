// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CanGrab.h"
#include "Attackable.h"
#include "Damageable.h"
#include "WukongAnimationInstance.generated.h"

/**
 * 
 */
UCLASS()
class WUKONG_API UWukongAnimationInstance : public UAnimInstance, public ICanGrab, public IAttackable, public IDamageable
{
	GENERATED_BODY()
	
	
	
	
};
