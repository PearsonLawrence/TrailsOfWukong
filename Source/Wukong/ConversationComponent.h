// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ConversationComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WUKONG_API UConversationComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UConversationComponent();


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UConversationComponent* OtherCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool ConversationLead;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
