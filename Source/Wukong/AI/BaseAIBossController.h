// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseAIBossController.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EGuardianStates : uint8
{
	IdleState 			    UMETA(DisplayName = "IdleState"),
	SeekState 				UMETA(DisplayName = "CombatState"),
	AvoidState 				UMETA(DisplayName = "AvoidState"),
	AttackState 			UMETA(DisplayName = "AttackState"),
	ActionState 			UMETA(DisplayName = "ActionState"),
	DodgeState  			UMETA(DisplayName = "DodgeState"),
	RageState 				UMETA(DisplayName = "RageState")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EBossAttackStates : uint8
{
	IdleState 			    UMETA(DisplayName = "IdleState"),
	SpinAttackState 		UMETA(DisplayName = "SpinAttackState"),
	AvoidBackState 			UMETA(DisplayName = "AvoidBackState"),
	AvoidLeftState 			UMETA(DisplayName = "AvoidLeftState"),
	AvoidRightState 	    UMETA(DisplayName = "AvoidRightState"),
	AttackLightState 	    UMETA(DisplayName = "AttackLightState"),
	LeapState 				UMETA(DisplayName = "LeapState"),
	StrafeLeft 				UMETA(DisplayName = "StrafeLeft"),
	StrafeRight 			UMETA(DisplayName = "StrafeRight"),
	WalkBack 				UMETA(DisplayName = "WalkBack"),
	WalkState 				UMETA(DisplayName = "WalkState")
};
/**
 * 
 */
UCLASS(config = Game)
class WUKONG_API ABaseAIBossController : public AAIController
{
	GENERATED_BODY()


public:
	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, Category = "target")
		EGuardianStates CurrentState;
	UPROPERTY(EditAnywhere, Category = "target")
		EBossAttackStates CurrentAttackSate;

	ABaseAIBossController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float AcceptanceRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		float RoamRadius;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		bool bIsActive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
		bool Attacking;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup") // consider EditDefaultsOnly
		bool Dodging;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		FVector MoveLocation;
	UPROPERTY(EditAnywhere, Category = "Setup") // consider EditDefaultsOnly
		FVector CachedLocation;
	UPROPERTY(EditAnywhere, Category = "target")
		AActor* TargetActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "target")
		float AttackCooldown;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "target")
		float DodgeCooldown;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "target")
		float attackTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "target")
		float MoveSwitchTime;

	UFUNCTION(BlueprintCallable)
		void DoChase(class AActor* target);
	UFUNCTION(BlueprintCallable)
		EGuardianStates GetCurrentState();
	UFUNCTION(BlueprintCallable)
		void SetCurrentState(EGuardianStates State);
	UFUNCTION(BlueprintCallable)
		void MoveTowards(FVector ToLocation, float MoveSpeed);
	UFUNCTION(BlueprintCallable)
		void LookTowards(FVector ToLocation, float TurnSpeed);



protected:
	class AAIBaseBoss* thisCharacter;
	class APawn* ThisPawn;
private:

	virtual void SetPawn(APawn* InPawn) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result) override;

	UFUNCTION()
		void OnPossesedAIDeath();

	////////////// State Actions
	void doIdle(float DT);
	void doTrack();
	void doCombat(float DT);
	void doAttack();
	void doAction();
	void distanceDecision();
	void closeDecision();

	void doAvoid();

	void HandleMovement();

	void doStrafe();
	void doDodge();
};
