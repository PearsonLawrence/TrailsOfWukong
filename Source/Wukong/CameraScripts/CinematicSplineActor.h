// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CinematicSplineActor.generated.h"

UCLASS()
class WUKONG_API ACinematicSplineActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACinematicSplineActor();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* HoldPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* Base;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* StoredActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USplineComponent* Spline;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool start;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PlayRate;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
