// All Rights Reserved by Punk Mage Games

#pragma once

#include "CoreMinimal.h"
#include "BossCombat.h"
#include "GameFramework/Character.h"
#include "Attackable.h"
#include "Damageable.h"
#include "AIBaseBoss.generated.h"




UCLASS(config = Game)
class WUKONG_API AAIBaseBoss : public ACharacter, public IBossCombat, public IDamageable
{
	GENERATED_BODY()

	AAIBaseBoss();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	// Sets default values for this character's properties

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = reference)
		class ABaseAIBossController* myAIController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
		class UHealthComponent* HealthComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
		class UCombatComponent* CombatComponent;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UWeaponMeshComponent* Weapon;

	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float RoamWalkSpeed;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float DodgeWalkSpeed;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float SprintWalkSpeed;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float AttackWalkSpeed;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float LeapWalkSpeed;
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		float SpinWalkSpeed;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		bool bIsActive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		AActor* BossTarget;

	UFUNCTION(BlueprintCallable)
		void SetBossActive(bool bActive, AActor* Target);



};
