// All Rights Reserved by Punk Mage Games

#include "SideScrollerCamera.h"
#include "Wukong/SunWukong.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ArrowComponent.h"
#include "CinematicSplineActor.h"
ASideScrollerCamera::ASideScrollerCamera()
{
	PrimaryActorTick.bCanEverTick = true;

	Zoffset = 100;
	Xoffset = 500;
	Yoffset = 0;
}

void ASideScrollerCamera::BeginPlay()
{
	Super::BeginPlay();

}

void ASideScrollerCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (followActor == nullptr) { return; }

	switch (followActor->CameraState)
	{
	    case ECameraStates::SideScrollerState:
			FollowMode(DeltaTime);
			break;

		case ECameraStates::LockedPoseandRotState:
			LockedMode(DeltaTime);
			break;

		case ECameraStates::LockedPoseandLookState:
			LockedandLookat(DeltaTime);
			break;

		case ECameraStates::LookFollowState:
			LookandFollowAtAngle(DeltaTime);
			break;

		case ECameraStates::SpiralStairCaseState:
			SpiralFollow(DeltaTime);
			break;
		case ECameraStates::PlayerControlCamera:
			PlayerControl(DeltaTime);
			break;
		case ECameraStates::BossCamState:
			BossCamera(DeltaTime);
			break;
		case ECameraStates::CinematicCameraState:
			CinematicCamera(DeltaTime);
			break;
		case ECameraStates::LockedPoseZState:
			LockedZPose(DeltaTime);
			break;
	}
}

void ASideScrollerCamera::FollowMode(float DeltaTime)
{
	FVector Location = (holdPoint.GetRotation().GetRightVector() * 600) + followActor->GetActorLocation();

	Location = Location + FVector(0, 0, Zoffset);

	SetActorLocation(FMath::VInterpTo(GetActorLocation(), Location, DeltaTime, 2));

	SetActorRotation(FMath::RInterpTo(GetActorRotation(), (holdPoint.GetRotation().GetRightVector() * -1).Rotation(), DeltaTime, 2));

}

void ASideScrollerCamera::LockedMode(float DeltaTime)
{
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), holdPoint.GetRotation().Rotator(), DeltaTime, 2));

	SetActorLocation(FMath::VInterpTo(GetActorLocation(), holdPoint.GetLocation(), DeltaTime, 1));
}

void ASideScrollerCamera::LockedandLookat(float DeltaTime)
{
	FVector Dir = (GetActorLocation() - followActor->GetActorLocation()).GetSafeNormal() * -1;

	SetActorLocation(FMath::VInterpTo(GetActorLocation(), holdPoint.GetLocation(), DeltaTime, 1));
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir.Rotation(), DeltaTime, 6));
}

void ASideScrollerCamera::LockedZPose(float DeltaTime)
{
	FVector Dir = (GetActorLocation() - followActor->GetActorLocation()).GetSafeNormal() * -1;

	FVector Location = FVector(holdPoint.GetLocation().X, holdPoint.GetLocation().Y, followActor->GetActorLocation().Z + Zoffset);

	SetActorLocation(FMath::VInterpTo(GetActorLocation(), Location, DeltaTime, 1));
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir.Rotation(), DeltaTime, 6));
}

void ASideScrollerCamera::LookandFollowAtAngle(float DeltaTime)
{
	FVector Location = (holdPoint.GetRotation().GetRightVector() * 600) + followActor->GetActorLocation();

	Location = Location + FVector(0, 0, Zoffset);

	SetActorLocation(FMath::VInterpTo(GetActorLocation(), Location, DeltaTime, 10));

	SetActorRotation(FMath::RInterpTo(GetActorRotation(), (holdPoint.GetRotation().GetRightVector() * -1).Rotation(), DeltaTime, 10));

}

void ASideScrollerCamera::SpiralFollow(float DeltaTime)
{
	FVector Location = (followActor->GetActorLocation() - FVector(holdPoint.GetLocation().X, holdPoint.GetLocation().Y, followActor->GetActorLocation().Z)).GetSafeNormal() * 600;
	FVector MoveLocation = followActor->GetActorLocation() + Location  + FVector(0,0,Zoffset);



	SetActorLocation(FMath::VInterpTo(GetActorLocation(), MoveLocation, DeltaTime, 4));

	FVector Dir = (followActor->GetActorLocation() - GetActorLocation()).GetSafeNormal();
	/*Dir.X = 0;
	Dir.Y = 0;*/

	FRotator Rot = FRotator(0, Dir.Rotation().Yaw,0);
	SetActorRotation(FMath::RInterpTo(GetActorRotation(), Rot, DeltaTime, 10));
}

void ASideScrollerCamera::PlayerControl(float DeltaTime)
{
	if (followActor->CamHoldPoint != nullptr)
	{
		SetActorLocation(FMath::VInterpTo(GetActorLocation(), followActor->CamHoldPoint->GetComponentLocation(), DeltaTime,8));

		FRotator Rot = followActor->CamHoldPoint->GetComponentRotation();
		Rot.Roll = 0;
		
		FRotator Dir = ((GetActorLocation() - followActor->GetActorLocation()).GetSafeNormal() * -1).Rotation();
		Dir.Roll = 0;
		SetActorRotation(FMath::RInterpTo(GetActorRotation(), Rot, DeltaTime, 5));
	}
	else
	{

	}
}

void ASideScrollerCamera::BossCamera(float DeltaTime)
{
	if (Boss != nullptr)
	{
		FVector BossPos = Boss->GetActorLocation() - FVector(0, 0, 150);
		FVector Dir = (BossPos - followActor->GetActorLocation()).GetSafeNormal();
		FRotator LookDir = Dir.Rotation();

		FHitResult HitBack;

		FCollisionShape Sphere;

		Sphere.SetSphere(20);

		FVector EndBack = (Dir * -550) + followActor->GetActorLocation() + FVector(0, 0, Zoffset);
		FVector EndFront = (GetActorForwardVector() * 200) + GetActorLocation();
		GetWorld()->SweepSingleByChannel(HitBack, followActor->GetActorLocation(), EndBack, GetActorRotation().Quaternion(),
			ECollisionChannel::ECC_GameTraceChannel11, Sphere);

		
		FVector TempPos = followActor->GetActorLocation();
		TempPos.Z = HitBack.ImpactPoint.Z;

		float HitBackDist = (HitBack.ImpactPoint - TempPos).Size();
		
		float Distance = (HitBack.bBlockingHit) ? -HitBackDist : -500;

		FVector Pos = (Dir * Distance) + followActor->GetActorLocation() + FVector(0,0,Zoffset);

		SetActorLocation(FMath::VInterpTo(GetActorLocation(), Pos, DeltaTime, 4));

		SetActorRotation(FMath::RInterpTo(GetActorRotation(), LookDir, DeltaTime, 2));

	}
}

void ASideScrollerCamera::CinematicCamera(float DeltaTime)
{
	if (CinematicLocation != nullptr)
	{
		FVector Dir = (GetActorLocation() - CinematicLocation->StoredActor->GetActorLocation()).GetSafeNormal() * -1;


		float DistanceToStart = (GetActorLocation() - CinematicLocation->HoldPoint->GetComponentLocation()).Size();
		(DistanceToStart <= 100.0f) ? CinematicLocation->start = true : CinematicLocation->start = false;

		SetActorLocation(FMath::VInterpTo(GetActorLocation(), CinematicLocation->HoldPoint->GetComponentLocation(), DeltaTime, 3));
		SetActorRotation(FMath::RInterpTo(GetActorRotation(), Dir.Rotation(), DeltaTime, 6));

	}
} 